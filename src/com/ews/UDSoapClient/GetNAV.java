
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFilRdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pFilDtStart" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="pFilDtEnd" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="pFilIdStart" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pFilIDEnd" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pFilRdpId",
    "pFilDtStart",
    "pFilDtEnd",
    "pFilIdStart",
    "pFilIDEnd"
})
@XmlRootElement(name = "getNAV")
public class GetNAV {

    protected String pSid;
    protected int pFilRdpId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pFilDtStart;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pFilDtEnd;
    protected int pFilIdStart;
    protected int pFilIDEnd;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pFilRdpId property.
     * 
     */
    public int getPFilRdpId() {
        return pFilRdpId;
    }

    /**
     * Sets the value of the pFilRdpId property.
     * 
     */
    public void setPFilRdpId(int value) {
        this.pFilRdpId = value;
    }

    /**
     * Gets the value of the pFilDtStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPFilDtStart() {
        return pFilDtStart;
    }

    /**
     * Sets the value of the pFilDtStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPFilDtStart(XMLGregorianCalendar value) {
        this.pFilDtStart = value;
    }

    /**
     * Gets the value of the pFilDtEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPFilDtEnd() {
        return pFilDtEnd;
    }

    /**
     * Sets the value of the pFilDtEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPFilDtEnd(XMLGregorianCalendar value) {
        this.pFilDtEnd = value;
    }

    /**
     * Gets the value of the pFilIdStart property.
     * 
     */
    public int getPFilIdStart() {
        return pFilIdStart;
    }

    /**
     * Sets the value of the pFilIdStart property.
     * 
     */
    public void setPFilIdStart(int value) {
        this.pFilIdStart = value;
    }

    /**
     * Gets the value of the pFilIDEnd property.
     * 
     */
    public int getPFilIDEnd() {
        return pFilIDEnd;
    }

    /**
     * Sets the value of the pFilIDEnd property.
     * 
     */
    public void setPFilIDEnd(int value) {
        this.pFilIDEnd = value;
    }

}
