
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSafeInvoiceAndPrintResult" type="{http://tempuri.org/}WetSafeSpeedInvoiceResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSafeInvoiceAndPrintResult"
})
@XmlRootElement(name = "setSafeInvoiceAndPrintResponse")
public class SetSafeInvoiceAndPrintResponse {

    protected WetSafeSpeedInvoiceResult setSafeInvoiceAndPrintResult;

    /**
     * Gets the value of the setSafeInvoiceAndPrintResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public WetSafeSpeedInvoiceResult getSetSafeInvoiceAndPrintResult() {
        return setSafeInvoiceAndPrintResult;
    }

    /**
     * Sets the value of the setSafeInvoiceAndPrintResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public void setSetSafeInvoiceAndPrintResult(WetSafeSpeedInvoiceResult value) {
        this.setSafeInvoiceAndPrintResult = value;
    }

}
