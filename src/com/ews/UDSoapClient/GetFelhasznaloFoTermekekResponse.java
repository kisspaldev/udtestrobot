
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getFelhasznaloFoTermekekResult" type="{http://tempuri.org/}rdsFelhasznaloFoTermekListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFelhasznaloFoTermekekResult"
})
@XmlRootElement(name = "getFelhasznaloFoTermekekResponse")
public class GetFelhasznaloFoTermekekResponse {

    protected RdsFelhasznaloFoTermekListResult getFelhasznaloFoTermekekResult;

    /**
     * Gets the value of the getFelhasznaloFoTermekekResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsFelhasznaloFoTermekListResult }
     *     
     */
    public RdsFelhasznaloFoTermekListResult getGetFelhasznaloFoTermekekResult() {
        return getFelhasznaloFoTermekekResult;
    }

    /**
     * Sets the value of the getFelhasznaloFoTermekekResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsFelhasznaloFoTermekListResult }
     *     
     */
    public void setGetFelhasznaloFoTermekekResult(RdsFelhasznaloFoTermekListResult value) {
        this.getFelhasznaloFoTermekekResult = value;
    }

}
