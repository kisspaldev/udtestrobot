
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSzerepkorokListResult" type="{http://tempuri.org/}rdsSzerepkorokListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSzerepkorokListResult"
})
@XmlRootElement(name = "getSzerepkorokListResponse")
public class GetSzerepkorokListResponse {

    protected RdsSzerepkorokListResult getSzerepkorokListResult;

    /**
     * Gets the value of the getSzerepkorokListResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsSzerepkorokListResult }
     *     
     */
    public RdsSzerepkorokListResult getGetSzerepkorokListResult() {
        return getSzerepkorokListResult;
    }

    /**
     * Sets the value of the getSzerepkorokListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsSzerepkorokListResult }
     *     
     */
    public void setGetSzerepkorokListResult(RdsSzerepkorokListResult value) {
        this.getSzerepkorokListResult = value;
    }

}
