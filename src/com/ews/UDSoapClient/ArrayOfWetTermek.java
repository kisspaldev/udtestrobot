
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetTermek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetTermek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WetTermek" type="{http://tempuri.org/}WetTermek" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetTermek", propOrder = {
    "wetTermek"
})
public class ArrayOfWetTermek {

    @XmlElement(name = "WetTermek", nillable = true)
    protected List<WetTermek> wetTermek;

    /**
     * Gets the value of the wetTermek property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetTermek property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetTermek().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetTermek }
     * 
     * 
     */
    public List<WetTermek> getWetTermek() {
        if (wetTermek == null) {
            wetTermek = new ArrayList<WetTermek>();
        }
        return this.wetTermek;
    }

}
