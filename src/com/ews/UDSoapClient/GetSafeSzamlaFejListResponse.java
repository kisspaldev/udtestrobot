
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSafeSzamlaFejListResult" type="{http://tempuri.org/}wetSafeSzamlaFejListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSafeSzamlaFejListResult"
})
@XmlRootElement(name = "getSafeSzamlaFejListResponse")
public class GetSafeSzamlaFejListResponse {

    protected WetSafeSzamlaFejListResult getSafeSzamlaFejListResult;

    /**
     * Gets the value of the getSafeSzamlaFejListResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeSzamlaFejListResult }
     *     
     */
    public WetSafeSzamlaFejListResult getGetSafeSzamlaFejListResult() {
        return getSafeSzamlaFejListResult;
    }

    /**
     * Sets the value of the getSafeSzamlaFejListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeSzamlaFejListResult }
     *     
     */
    public void setGetSafeSzamlaFejListResult(WetSafeSzamlaFejListResult value) {
        this.getSafeSzamlaFejListResult = value;
    }

}
