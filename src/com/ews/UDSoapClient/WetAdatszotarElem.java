
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetAdatszotarElem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetAdatszotarElem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="radKulcs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="radErtek" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="radSzamErtek" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetAdatszotarElem", propOrder = {
    "radKulcs",
    "radErtek",
    "radSzamErtek"
})
public class WetAdatszotarElem {

    protected String radKulcs;
    protected String radErtek;
    protected double radSzamErtek;

    /**
     * Gets the value of the radKulcs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadKulcs() {
        return radKulcs;
    }

    /**
     * Sets the value of the radKulcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadKulcs(String value) {
        this.radKulcs = value;
    }

    /**
     * Gets the value of the radErtek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadErtek() {
        return radErtek;
    }

    /**
     * Sets the value of the radErtek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadErtek(String value) {
        this.radErtek = value;
    }

    /**
     * Gets the value of the radSzamErtek property.
     * 
     */
    public double getRadSzamErtek() {
        return radSzamErtek;
    }

    /**
     * Sets the value of the radSzamErtek property.
     * 
     */
    public void setRadSzamErtek(double value) {
        this.radSzamErtek = value;
    }

}
