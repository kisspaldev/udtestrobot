
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetUrlapEllenorzesElem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetUrlapEllenorzesElem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WetUrlapEllenorzesElem" type="{http://tempuri.org/}WetUrlapEllenorzesElem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetUrlapEllenorzesElem", propOrder = {
    "wetUrlapEllenorzesElem"
})
public class ArrayOfWetUrlapEllenorzesElem {

    @XmlElement(name = "WetUrlapEllenorzesElem", nillable = true)
    protected List<WetUrlapEllenorzesElem> wetUrlapEllenorzesElem;

    /**
     * Gets the value of the wetUrlapEllenorzesElem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetUrlapEllenorzesElem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetUrlapEllenorzesElem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetUrlapEllenorzesElem }
     * 
     * 
     */
    public List<WetUrlapEllenorzesElem> getWetUrlapEllenorzesElem() {
        if (wetUrlapEllenorzesElem == null) {
            wetUrlapEllenorzesElem = new ArrayList<WetUrlapEllenorzesElem>();
        }
        return this.wetUrlapEllenorzesElem;
    }

}
