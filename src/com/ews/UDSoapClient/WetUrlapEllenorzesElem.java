
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetUrlapEllenorzesElem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetUrlapEllenorzesElem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="urlapElemNeve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="problemaLeirasa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetUrlapEllenorzesElem", propOrder = {
    "urlapElemNeve",
    "problemaLeirasa"
})
public class WetUrlapEllenorzesElem {

    protected String urlapElemNeve;
    protected String problemaLeirasa;

    /**
     * Gets the value of the urlapElemNeve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlapElemNeve() {
        return urlapElemNeve;
    }

    /**
     * Sets the value of the urlapElemNeve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlapElemNeve(String value) {
        this.urlapElemNeve = value;
    }

    /**
     * Gets the value of the problemaLeirasa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProblemaLeirasa() {
        return problemaLeirasa;
    }

    /**
     * Sets the value of the problemaLeirasa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProblemaLeirasa(String value) {
        this.problemaLeirasa = value;
    }

}
