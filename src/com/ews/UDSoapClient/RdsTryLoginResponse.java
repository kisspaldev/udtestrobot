
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdsTryLoginResult" type="{http://tempuri.org/}tryLoginResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rdsTryLoginResult"
})
@XmlRootElement(name = "rdsTryLoginResponse")
public class RdsTryLoginResponse {

    protected TryLoginResult rdsTryLoginResult;

    /**
     * Gets the value of the rdsTryLoginResult property.
     * 
     * @return
     *     possible object is
     *     {@link TryLoginResult }
     *     
     */
    public TryLoginResult getRdsTryLoginResult() {
        return rdsTryLoginResult;
    }

    /**
     * Sets the value of the rdsTryLoginResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TryLoginResult }
     *     
     */
    public void setRdsTryLoginResult(TryLoginResult value) {
        this.rdsTryLoginResult = value;
    }

}
