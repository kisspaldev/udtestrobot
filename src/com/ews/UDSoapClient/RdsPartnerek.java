
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RdsPartnerek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RdsPartnerek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rdpAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpRdpid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpAdoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpEuadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpBankszamla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpOrszag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpIranyitoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpVaros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpUtcahazszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpNyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpAktiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpFelhasznalo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpRdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpRdpAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RdsPartnerek", propOrder = {
    "rdpId",
    "rdpAzon",
    "rdpRdpid",
    "rdpNev",
    "rdpAdoszam",
    "rdpEuadoszam",
    "rdpBankszamla",
    "rdpOrszag",
    "rdpIranyitoszam",
    "rdpVaros",
    "rdpUtcahazszam",
    "rdpNyelv",
    "rdpAktiv",
    "rdpFelhasznalo",
    "rdpRdpNev",
    "rdpRdpAzon"
})
public class RdsPartnerek {

    protected int rdpId;
    protected String rdpAzon;
    protected int rdpRdpid;
    protected String rdpNev;
    protected String rdpAdoszam;
    protected String rdpEuadoszam;
    protected String rdpBankszamla;
    protected String rdpOrszag;
    protected String rdpIranyitoszam;
    protected String rdpVaros;
    protected String rdpUtcahazszam;
    protected String rdpNyelv;
    protected String rdpAktiv;
    protected String rdpFelhasznalo;
    protected String rdpRdpNev;
    protected String rdpRdpAzon;

    /**
     * Gets the value of the rdpId property.
     * 
     */
    public int getRdpId() {
        return rdpId;
    }

    /**
     * Sets the value of the rdpId property.
     * 
     */
    public void setRdpId(int value) {
        this.rdpId = value;
    }

    /**
     * Gets the value of the rdpAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAzon() {
        return rdpAzon;
    }

    /**
     * Sets the value of the rdpAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAzon(String value) {
        this.rdpAzon = value;
    }

    /**
     * Gets the value of the rdpRdpid property.
     * 
     */
    public int getRdpRdpid() {
        return rdpRdpid;
    }

    /**
     * Sets the value of the rdpRdpid property.
     * 
     */
    public void setRdpRdpid(int value) {
        this.rdpRdpid = value;
    }

    /**
     * Gets the value of the rdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpNev() {
        return rdpNev;
    }

    /**
     * Sets the value of the rdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpNev(String value) {
        this.rdpNev = value;
    }

    /**
     * Gets the value of the rdpAdoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAdoszam() {
        return rdpAdoszam;
    }

    /**
     * Sets the value of the rdpAdoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAdoszam(String value) {
        this.rdpAdoszam = value;
    }

    /**
     * Gets the value of the rdpEuadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpEuadoszam() {
        return rdpEuadoszam;
    }

    /**
     * Sets the value of the rdpEuadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpEuadoszam(String value) {
        this.rdpEuadoszam = value;
    }

    /**
     * Gets the value of the rdpBankszamla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpBankszamla() {
        return rdpBankszamla;
    }

    /**
     * Sets the value of the rdpBankszamla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpBankszamla(String value) {
        this.rdpBankszamla = value;
    }

    /**
     * Gets the value of the rdpOrszag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpOrszag() {
        return rdpOrszag;
    }

    /**
     * Sets the value of the rdpOrszag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpOrszag(String value) {
        this.rdpOrszag = value;
    }

    /**
     * Gets the value of the rdpIranyitoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpIranyitoszam() {
        return rdpIranyitoszam;
    }

    /**
     * Sets the value of the rdpIranyitoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpIranyitoszam(String value) {
        this.rdpIranyitoszam = value;
    }

    /**
     * Gets the value of the rdpVaros property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpVaros() {
        return rdpVaros;
    }

    /**
     * Sets the value of the rdpVaros property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpVaros(String value) {
        this.rdpVaros = value;
    }

    /**
     * Gets the value of the rdpUtcahazszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpUtcahazszam() {
        return rdpUtcahazszam;
    }

    /**
     * Sets the value of the rdpUtcahazszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpUtcahazszam(String value) {
        this.rdpUtcahazszam = value;
    }

    /**
     * Gets the value of the rdpNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpNyelv() {
        return rdpNyelv;
    }

    /**
     * Sets the value of the rdpNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpNyelv(String value) {
        this.rdpNyelv = value;
    }

    /**
     * Gets the value of the rdpAktiv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAktiv() {
        return rdpAktiv;
    }

    /**
     * Sets the value of the rdpAktiv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAktiv(String value) {
        this.rdpAktiv = value;
    }

    /**
     * Gets the value of the rdpFelhasznalo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpFelhasznalo() {
        return rdpFelhasznalo;
    }

    /**
     * Sets the value of the rdpFelhasznalo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpFelhasznalo(String value) {
        this.rdpFelhasznalo = value;
    }

    /**
     * Gets the value of the rdpRdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpRdpNev() {
        return rdpRdpNev;
    }

    /**
     * Sets the value of the rdpRdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpRdpNev(String value) {
        this.rdpRdpNev = value;
    }

    /**
     * Gets the value of the rdpRdpAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpRdpAzon() {
        return rdpRdpAzon;
    }

    /**
     * Sets the value of the rdpRdpAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpRdpAzon(String value) {
        this.rdpRdpAzon = value;
    }

}
