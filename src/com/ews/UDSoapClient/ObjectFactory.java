
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ews.UDSoapClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ews.UDSoapClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTermekResponse }
     * 
     */
    public GetTermekResponse createGetTermekResponse() {
        return new GetTermekResponse();
    }

    /**
     * Create an instance of {@link WetTermek }
     * 
     */
    public WetTermek createWetTermek() {
        return new WetTermek();
    }

    /**
     * Create an instance of {@link GetNAVResponse }
     * 
     */
    public GetNAVResponse createGetNAVResponse() {
        return new GetNAVResponse();
    }

    /**
     * Create an instance of {@link GetNaploList }
     * 
     */
    public GetNaploList createGetNaploList() {
        return new GetNaploList();
    }

    /**
     * Create an instance of {@link SetSafeBill }
     * 
     */
    public SetSafeBill createSetSafeBill() {
        return new SetSafeBill();
    }

    /**
     * Create an instance of {@link WetSafeBillInput }
     * 
     */
    public WetSafeBillInput createWetSafeBillInput() {
        return new WetSafeBillInput();
    }

    /**
     * Create an instance of {@link SetPartnerResponse }
     * 
     */
    public SetPartnerResponse createSetPartnerResponse() {
        return new SetPartnerResponse();
    }

    /**
     * Create an instance of {@link WebUrlapEllenorzesResult }
     * 
     */
    public WebUrlapEllenorzesResult createWebUrlapEllenorzesResult() {
        return new WebUrlapEllenorzesResult();
    }

    /**
     * Create an instance of {@link SetSafeInvoiceAndPrint }
     * 
     */
    public SetSafeInvoiceAndPrint createSetSafeInvoiceAndPrint() {
        return new SetSafeInvoiceAndPrint();
    }

    /**
     * Create an instance of {@link WetSzamlaFej }
     * 
     */
    public WetSzamlaFej createWetSzamlaFej() {
        return new WetSzamlaFej();
    }

    /**
     * Create an instance of {@link GetAdatszotarListResponse }
     * 
     */
    public GetAdatszotarListResponse createGetAdatszotarListResponse() {
        return new GetAdatszotarListResponse();
    }

    /**
     * Create an instance of {@link WetAdatszotarElemResult }
     * 
     */
    public WetAdatszotarElemResult createWetAdatszotarElemResult() {
        return new WetAdatszotarElemResult();
    }

    /**
     * Create an instance of {@link SetDelSzamlaTervResponse }
     * 
     */
    public SetDelSzamlaTervResponse createSetDelSzamlaTervResponse() {
        return new SetDelSzamlaTervResponse();
    }

    /**
     * Create an instance of {@link SetTermekResponse }
     * 
     */
    public SetTermekResponse createSetTermekResponse() {
        return new SetTermekResponse();
    }

    /**
     * Create an instance of {@link GetNyugtaUjResponse }
     * 
     */
    public GetNyugtaUjResponse createGetNyugtaUjResponse() {
        return new GetNyugtaUjResponse();
    }

    /**
     * Create an instance of {@link GetUpdateClientResponse }
     * 
     */
    public GetUpdateClientResponse createGetUpdateClientResponse() {
        return new GetUpdateClientResponse();
    }

    /**
     * Create an instance of {@link SetTermek }
     * 
     */
    public SetTermek createSetTermek() {
        return new SetTermek();
    }

    /**
     * Create an instance of {@link GetPartner }
     * 
     */
    public GetPartner createGetPartner() {
        return new GetPartner();
    }

    /**
     * Create an instance of {@link SetFelhasznaloDel }
     * 
     */
    public SetFelhasznaloDel createSetFelhasznaloDel() {
        return new SetFelhasznaloDel();
    }

    /**
     * Create an instance of {@link GetTermekListResponse }
     * 
     */
    public GetTermekListResponse createGetTermekListResponse() {
        return new GetTermekListResponse();
    }

    /**
     * Create an instance of {@link WetTermekekListResult }
     * 
     */
    public WetTermekekListResult createWetTermekekListResult() {
        return new WetTermekekListResult();
    }

    /**
     * Create an instance of {@link SetInitFelhasznaloResponse }
     * 
     */
    public SetInitFelhasznaloResponse createSetInitFelhasznaloResponse() {
        return new SetInitFelhasznaloResponse();
    }

    /**
     * Create an instance of {@link SetSzamlaResponse }
     * 
     */
    public SetSzamlaResponse createSetSzamlaResponse() {
        return new SetSzamlaResponse();
    }

    /**
     * Create an instance of {@link GetSzamlaFejList }
     * 
     */
    public GetSzamlaFejList createGetSzamlaFejList() {
        return new GetSzamlaFejList();
    }

    /**
     * Create an instance of {@link GetFormAdatszotarListResponse }
     * 
     */
    public GetFormAdatszotarListResponse createGetFormAdatszotarListResponse() {
        return new GetFormAdatszotarListResponse();
    }

    /**
     * Create an instance of {@link GetSzamlaReportAdatszotarListResponse }
     * 
     */
    public GetSzamlaReportAdatszotarListResponse createGetSzamlaReportAdatszotarListResponse() {
        return new GetSzamlaReportAdatszotarListResponse();
    }

    /**
     * Create an instance of {@link SetSajatJelszo }
     * 
     */
    public SetSajatJelszo createSetSajatJelszo() {
        return new SetSajatJelszo();
    }

    /**
     * Create an instance of {@link SetInitFelhasznalo }
     * 
     */
    public SetInitFelhasznalo createSetInitFelhasznalo() {
        return new SetInitFelhasznalo();
    }

    /**
     * Create an instance of {@link GetTmpSzamlaTetelListResponse }
     * 
     */
    public GetTmpSzamlaTetelListResponse createGetTmpSzamlaTetelListResponse() {
        return new GetTmpSzamlaTetelListResponse();
    }

    /**
     * Create an instance of {@link WetSzamlaTetelListResult }
     * 
     */
    public WetSzamlaTetelListResult createWetSzamlaTetelListResult() {
        return new WetSzamlaTetelListResult();
    }

    /**
     * Create an instance of {@link GetSzamlaFejListResponse }
     * 
     */
    public GetSzamlaFejListResponse createGetSzamlaFejListResponse() {
        return new GetSzamlaFejListResponse();
    }

    /**
     * Create an instance of {@link WetSzamlaFejListResult }
     * 
     */
    public WetSzamlaFejListResult createWetSzamlaFejListResult() {
        return new WetSzamlaFejListResult();
    }

    /**
     * Create an instance of {@link SetTermekDelResponse }
     * 
     */
    public SetTermekDelResponse createSetTermekDelResponse() {
        return new SetTermekDelResponse();
    }

    /**
     * Create an instance of {@link SetTmpSzamlaTetel }
     * 
     */
    public SetTmpSzamlaTetel createSetTmpSzamlaTetel() {
        return new SetTmpSzamlaTetel();
    }

    /**
     * Create an instance of {@link WetSzamlaTetel }
     * 
     */
    public WetSzamlaTetel createWetSzamlaTetel() {
        return new WetSzamlaTetel();
    }

    /**
     * Create an instance of {@link GetFelhasznalokListResponse }
     * 
     */
    public GetFelhasznalokListResponse createGetFelhasznalokListResponse() {
        return new GetFelhasznalokListResponse();
    }

    /**
     * Create an instance of {@link RdsFelhasznalokListResult }
     * 
     */
    public RdsFelhasznalokListResult createRdsFelhasznalokListResult() {
        return new RdsFelhasznalokListResult();
    }

    /**
     * Create an instance of {@link GetNaploListResponse }
     * 
     */
    public GetNaploListResponse createGetNaploListResponse() {
        return new GetNaploListResponse();
    }

    /**
     * Create an instance of {@link RdsNaploListResult }
     * 
     */
    public RdsNaploListResult createRdsNaploListResult() {
        return new RdsNaploListResult();
    }

    /**
     * Create an instance of {@link SetMoveUpFelhasznaloTermekSzla }
     * 
     */
    public SetMoveUpFelhasznaloTermekSzla createSetMoveUpFelhasznaloTermekSzla() {
        return new SetMoveUpFelhasznaloTermekSzla();
    }

    /**
     * Create an instance of {@link SetSajatJelszoResponse }
     * 
     */
    public SetSajatJelszoResponse createSetSajatJelszoResponse() {
        return new SetSajatJelszoResponse();
    }

    /**
     * Create an instance of {@link GetFelhasznalo }
     * 
     */
    public GetFelhasznalo createGetFelhasznalo() {
        return new GetFelhasznalo();
    }

    /**
     * Create an instance of {@link GetAdatszotarList }
     * 
     */
    public GetAdatszotarList createGetAdatszotarList() {
        return new GetAdatszotarList();
    }

    /**
     * Create an instance of {@link SetMoveDownFelhasznaloTermekSzlaResponse }
     * 
     */
    public SetMoveDownFelhasznaloTermekSzlaResponse createSetMoveDownFelhasznaloTermekSzlaResponse() {
        return new SetMoveDownFelhasznaloTermekSzlaResponse();
    }

    /**
     * Create an instance of {@link GetPartnerListResponse }
     * 
     */
    public GetPartnerListResponse createGetPartnerListResponse() {
        return new GetPartnerListResponse();
    }

    /**
     * Create an instance of {@link RdsPartnerekListResult }
     * 
     */
    public RdsPartnerekListResult createRdsPartnerekListResult() {
        return new RdsPartnerekListResult();
    }

    /**
     * Create an instance of {@link GetMainPartner }
     * 
     */
    public GetMainPartner createGetMainPartner() {
        return new GetMainPartner();
    }

    /**
     * Create an instance of {@link GetUpdateClient }
     * 
     */
    public GetUpdateClient createGetUpdateClient() {
        return new GetUpdateClient();
    }

    /**
     * Create an instance of {@link GetSzerepkorokList }
     * 
     */
    public GetSzerepkorokList createGetSzerepkorokList() {
        return new GetSzerepkorokList();
    }

    /**
     * Create an instance of {@link RdsLogoutResponse }
     * 
     */
    public RdsLogoutResponse createRdsLogoutResponse() {
        return new RdsLogoutResponse();
    }

    /**
     * Create an instance of {@link SetMoveDownFelhasznaloTermekResponse }
     * 
     */
    public SetMoveDownFelhasznaloTermekResponse createSetMoveDownFelhasznaloTermekResponse() {
        return new SetMoveDownFelhasznaloTermekResponse();
    }

    /**
     * Create an instance of {@link SetMoveUpFotermekResponse }
     * 
     */
    public SetMoveUpFotermekResponse createSetMoveUpFotermekResponse() {
        return new SetMoveUpFotermekResponse();
    }

    /**
     * Create an instance of {@link SetFelhasznaloDelResponse }
     * 
     */
    public SetFelhasznaloDelResponse createSetFelhasznaloDelResponse() {
        return new SetFelhasznaloDelResponse();
    }

    /**
     * Create an instance of {@link SetUnAssignFelhasznaloTermek }
     * 
     */
    public SetUnAssignFelhasznaloTermek createSetUnAssignFelhasznaloTermek() {
        return new SetUnAssignFelhasznaloTermek();
    }

    /**
     * Create an instance of {@link SetMoveUpFelhasznaloTermekSzlaResponse }
     * 
     */
    public SetMoveUpFelhasznaloTermekSzlaResponse createSetMoveUpFelhasznaloTermekSzlaResponse() {
        return new SetMoveUpFelhasznaloTermekSzlaResponse();
    }

    /**
     * Create an instance of {@link SetSafeInvoiceAndPrintResponse }
     * 
     */
    public SetSafeInvoiceAndPrintResponse createSetSafeInvoiceAndPrintResponse() {
        return new SetSafeInvoiceAndPrintResponse();
    }

    /**
     * Create an instance of {@link WetSafeSpeedInvoiceResult }
     * 
     */
    public WetSafeSpeedInvoiceResult createWetSafeSpeedInvoiceResult() {
        return new WetSafeSpeedInvoiceResult();
    }

    /**
     * Create an instance of {@link SetSafeSpeedBill }
     * 
     */
    public SetSafeSpeedBill createSetSafeSpeedBill() {
        return new SetSafeSpeedBill();
    }

    /**
     * Create an instance of {@link WetSafeSpeedBillInput }
     * 
     */
    public WetSafeSpeedBillInput createWetSafeSpeedBillInput() {
        return new WetSafeSpeedBillInput();
    }

    /**
     * Create an instance of {@link SetSafeSpeedBillResponse }
     * 
     */
    public SetSafeSpeedBillResponse createSetSafeSpeedBillResponse() {
        return new SetSafeSpeedBillResponse();
    }

    /**
     * Create an instance of {@link GetNyelvekList }
     * 
     */
    public GetNyelvekList createGetNyelvekList() {
        return new GetNyelvekList();
    }

    /**
     * Create an instance of {@link SetMoveDownFelhasznaloTermek }
     * 
     */
    public SetMoveDownFelhasznaloTermek createSetMoveDownFelhasznaloTermek() {
        return new SetMoveDownFelhasznaloTermek();
    }

    /**
     * Create an instance of {@link GetNAV }
     * 
     */
    public GetNAV createGetNAV() {
        return new GetNAV();
    }

    /**
     * Create an instance of {@link GetTmpSzamlaTetelResponse }
     * 
     */
    public GetTmpSzamlaTetelResponse createGetTmpSzamlaTetelResponse() {
        return new GetTmpSzamlaTetelResponse();
    }

    /**
     * Create an instance of {@link GetLoginedPartner }
     * 
     */
    public GetLoginedPartner createGetLoginedPartner() {
        return new GetLoginedPartner();
    }

    /**
     * Create an instance of {@link SetSafeSpeedInvoiceResponse }
     * 
     */
    public SetSafeSpeedInvoiceResponse createSetSafeSpeedInvoiceResponse() {
        return new SetSafeSpeedInvoiceResponse();
    }

    /**
     * Create an instance of {@link SetSafeInvoiceAndPrintByWsfIdResponse }
     * 
     */
    public SetSafeInvoiceAndPrintByWsfIdResponse createSetSafeInvoiceAndPrintByWsfIdResponse() {
        return new SetSafeInvoiceAndPrintByWsfIdResponse();
    }

    /**
     * Create an instance of {@link GetInitSzamlaForm }
     * 
     */
    public GetInitSzamlaForm createGetInitSzamlaForm() {
        return new GetInitSzamlaForm();
    }

    /**
     * Create an instance of {@link SetFelhasznalo }
     * 
     */
    public SetFelhasznalo createSetFelhasznalo() {
        return new SetFelhasznalo();
    }

    /**
     * Create an instance of {@link RdsFelhasznalok }
     * 
     */
    public RdsFelhasznalok createRdsFelhasznalok() {
        return new RdsFelhasznalok();
    }

    /**
     * Create an instance of {@link GetSpeedButtonTermekList }
     * 
     */
    public GetSpeedButtonTermekList createGetSpeedButtonTermekList() {
        return new GetSpeedButtonTermekList();
    }

    /**
     * Create an instance of {@link SetUnAssignFelhasznaloTermekSzlaResponse }
     * 
     */
    public SetUnAssignFelhasznaloTermekSzlaResponse createSetUnAssignFelhasznaloTermekSzlaResponse() {
        return new SetUnAssignFelhasznaloTermekSzlaResponse();
    }

    /**
     * Create an instance of {@link GetFelhasznalokList }
     * 
     */
    public GetFelhasznalokList createGetFelhasznalokList() {
        return new GetFelhasznalokList();
    }

    /**
     * Create an instance of {@link GetPartnerResponse }
     * 
     */
    public GetPartnerResponse createGetPartnerResponse() {
        return new GetPartnerResponse();
    }

    /**
     * Create an instance of {@link RdsPartnerek }
     * 
     */
    public RdsPartnerek createRdsPartnerek() {
        return new RdsPartnerek();
    }

    /**
     * Create an instance of {@link SetAssignFelhasznaloTermekSzla }
     * 
     */
    public SetAssignFelhasznaloTermekSzla createSetAssignFelhasznaloTermekSzla() {
        return new SetAssignFelhasznaloTermekSzla();
    }

    /**
     * Create an instance of {@link SetSafeBillResponse }
     * 
     */
    public SetSafeBillResponse createSetSafeBillResponse() {
        return new SetSafeBillResponse();
    }

    /**
     * Create an instance of {@link RdsTryLogin }
     * 
     */
    public RdsTryLogin createRdsTryLogin() {
        return new RdsTryLogin();
    }

    /**
     * Create an instance of {@link GetNyugtaUj }
     * 
     */
    public GetNyugtaUj createGetNyugtaUj() {
        return new GetNyugtaUj();
    }

    /**
     * Create an instance of {@link SetPartnerkDelResponse }
     * 
     */
    public SetPartnerkDelResponse createSetPartnerkDelResponse() {
        return new SetPartnerkDelResponse();
    }

    /**
     * Create an instance of {@link GetFelhasznaloTermekekSzlaResponse }
     * 
     */
    public GetFelhasznaloTermekekSzlaResponse createGetFelhasznaloTermekekSzlaResponse() {
        return new GetFelhasznaloTermekekSzlaResponse();
    }

    /**
     * Create an instance of {@link RdsFelhasznaloTermekListResult }
     * 
     */
    public RdsFelhasznaloTermekListResult createRdsFelhasznaloTermekListResult() {
        return new RdsFelhasznaloTermekListResult();
    }

    /**
     * Create an instance of {@link GetFelhasznaloTermekek }
     * 
     */
    public GetFelhasznaloTermekek createGetFelhasznaloTermekek() {
        return new GetFelhasznaloTermekek();
    }

    /**
     * Create an instance of {@link GetFelhasznaloFoTermekekResponse }
     * 
     */
    public GetFelhasznaloFoTermekekResponse createGetFelhasznaloFoTermekekResponse() {
        return new GetFelhasznaloFoTermekekResponse();
    }

    /**
     * Create an instance of {@link RdsFelhasznaloFoTermekListResult }
     * 
     */
    public RdsFelhasznaloFoTermekListResult createRdsFelhasznaloFoTermekListResult() {
        return new RdsFelhasznaloFoTermekListResult();
    }

    /**
     * Create an instance of {@link SetMoveDownFelhasznaloTermekSzla }
     * 
     */
    public SetMoveDownFelhasznaloTermekSzla createSetMoveDownFelhasznaloTermekSzla() {
        return new SetMoveDownFelhasznaloTermekSzla();
    }

    /**
     * Create an instance of {@link SetUnAssignFelhasznaloTermekResponse }
     * 
     */
    public SetUnAssignFelhasznaloTermekResponse createSetUnAssignFelhasznaloTermekResponse() {
        return new SetUnAssignFelhasznaloTermekResponse();
    }

    /**
     * Create an instance of {@link SetSafeStornoInvoiceAndPrintResponse }
     * 
     */
    public SetSafeStornoInvoiceAndPrintResponse createSetSafeStornoInvoiceAndPrintResponse() {
        return new SetSafeStornoInvoiceAndPrintResponse();
    }

    /**
     * Create an instance of {@link GetFormAdatszotarList }
     * 
     */
    public GetFormAdatszotarList createGetFormAdatszotarList() {
        return new GetFormAdatszotarList();
    }

    /**
     * Create an instance of {@link GetClientAppVersion }
     * 
     */
    public GetClientAppVersion createGetClientAppVersion() {
        return new GetClientAppVersion();
    }

    /**
     * Create an instance of {@link GetSzamlaTetelList }
     * 
     */
    public GetSzamlaTetelList createGetSzamlaTetelList() {
        return new GetSzamlaTetelList();
    }

    /**
     * Create an instance of {@link SetTmpSzamlaTetelResponse }
     * 
     */
    public SetTmpSzamlaTetelResponse createSetTmpSzamlaTetelResponse() {
        return new SetTmpSzamlaTetelResponse();
    }

    /**
     * Create an instance of {@link GetFelhasznaloFoTermekek }
     * 
     */
    public GetFelhasznaloFoTermekek createGetFelhasznaloFoTermekek() {
        return new GetFelhasznaloFoTermekek();
    }

    /**
     * Create an instance of {@link SetSafeSpeedInvoice }
     * 
     */
    public SetSafeSpeedInvoice createSetSafeSpeedInvoice() {
        return new SetSafeSpeedInvoice();
    }

    /**
     * Create an instance of {@link WetSafeSpeedInvoiceInput }
     * 
     */
    public WetSafeSpeedInvoiceInput createWetSafeSpeedInvoiceInput() {
        return new WetSafeSpeedInvoiceInput();
    }

    /**
     * Create an instance of {@link SetUnAssignFelhasznaloTermekSzla }
     * 
     */
    public SetUnAssignFelhasznaloTermekSzla createSetUnAssignFelhasznaloTermekSzla() {
        return new SetUnAssignFelhasznaloTermekSzla();
    }

    /**
     * Create an instance of {@link SetAssignFelhasznaloTermekSzlaResponse }
     * 
     */
    public SetAssignFelhasznaloTermekSzlaResponse createSetAssignFelhasznaloTermekSzlaResponse() {
        return new SetAssignFelhasznaloTermekSzlaResponse();
    }

    /**
     * Create an instance of {@link RdsLogout }
     * 
     */
    public RdsLogout createRdsLogout() {
        return new RdsLogout();
    }

    /**
     * Create an instance of {@link SetSafePrintInvoiceResponse }
     * 
     */
    public SetSafePrintInvoiceResponse createSetSafePrintInvoiceResponse() {
        return new SetSafePrintInvoiceResponse();
    }

    /**
     * Create an instance of {@link WetSafeInvoiceDetailResult }
     * 
     */
    public WetSafeInvoiceDetailResult createWetSafeInvoiceDetailResult() {
        return new WetSafeInvoiceDetailResult();
    }

    /**
     * Create an instance of {@link SetMoveDownFotermekResponse }
     * 
     */
    public SetMoveDownFotermekResponse createSetMoveDownFotermekResponse() {
        return new SetMoveDownFotermekResponse();
    }

    /**
     * Create an instance of {@link SetTermekDel }
     * 
     */
    public SetTermekDel createSetTermekDel() {
        return new SetTermekDel();
    }

    /**
     * Create an instance of {@link GetFelhasznaloResponse }
     * 
     */
    public GetFelhasznaloResponse createGetFelhasznaloResponse() {
        return new GetFelhasznaloResponse();
    }

    /**
     * Create an instance of {@link GetInitSzamlaFormResponse }
     * 
     */
    public GetInitSzamlaFormResponse createGetInitSzamlaFormResponse() {
        return new GetInitSzamlaFormResponse();
    }

    /**
     * Create an instance of {@link WetSzamlaUrlapResult }
     * 
     */
    public WetSzamlaUrlapResult createWetSzamlaUrlapResult() {
        return new WetSzamlaUrlapResult();
    }

    /**
     * Create an instance of {@link GetSafeSzamlaFejListResponse }
     * 
     */
    public GetSafeSzamlaFejListResponse createGetSafeSzamlaFejListResponse() {
        return new GetSafeSzamlaFejListResponse();
    }

    /**
     * Create an instance of {@link WetSafeSzamlaFejListResult }
     * 
     */
    public WetSafeSzamlaFejListResult createWetSafeSzamlaFejListResult() {
        return new WetSafeSzamlaFejListResult();
    }

    /**
     * Create an instance of {@link GetPartnerList }
     * 
     */
    public GetPartnerList createGetPartnerList() {
        return new GetPartnerList();
    }

    /**
     * Create an instance of {@link SetDelSzamlaTerv }
     * 
     */
    public SetDelSzamlaTerv createSetDelSzamlaTerv() {
        return new SetDelSzamlaTerv();
    }

    /**
     * Create an instance of {@link GetSzamlaReportAdatszotarList }
     * 
     */
    public GetSzamlaReportAdatszotarList createGetSzamlaReportAdatszotarList() {
        return new GetSzamlaReportAdatszotarList();
    }

    /**
     * Create an instance of {@link SetTmpDelSzamlaTetelResponse }
     * 
     */
    public SetTmpDelSzamlaTetelResponse createSetTmpDelSzamlaTetelResponse() {
        return new SetTmpDelSzamlaTetelResponse();
    }

    /**
     * Create an instance of {@link GetTmpSzamlaTetel }
     * 
     */
    public GetTmpSzamlaTetel createGetTmpSzamlaTetel() {
        return new GetTmpSzamlaTetel();
    }

    /**
     * Create an instance of {@link GetLoginedPartnerResponse }
     * 
     */
    public GetLoginedPartnerResponse createGetLoginedPartnerResponse() {
        return new GetLoginedPartnerResponse();
    }

    /**
     * Create an instance of {@link SetAssignFelhasznaloTermek }
     * 
     */
    public SetAssignFelhasznaloTermek createSetAssignFelhasznaloTermek() {
        return new SetAssignFelhasznaloTermek();
    }

    /**
     * Create an instance of {@link GetSzerepkorokListResponse }
     * 
     */
    public GetSzerepkorokListResponse createGetSzerepkorokListResponse() {
        return new GetSzerepkorokListResponse();
    }

    /**
     * Create an instance of {@link RdsSzerepkorokListResult }
     * 
     */
    public RdsSzerepkorokListResult createRdsSzerepkorokListResult() {
        return new RdsSzerepkorokListResult();
    }

    /**
     * Create an instance of {@link GetInitNyugtaFormResponse }
     * 
     */
    public GetInitNyugtaFormResponse createGetInitNyugtaFormResponse() {
        return new GetInitNyugtaFormResponse();
    }

    /**
     * Create an instance of {@link WetNyugtaUrlapResult }
     * 
     */
    public WetNyugtaUrlapResult createWetNyugtaUrlapResult() {
        return new WetNyugtaUrlapResult();
    }

    /**
     * Create an instance of {@link GetTermek }
     * 
     */
    public GetTermek createGetTermek() {
        return new GetTermek();
    }

    /**
     * Create an instance of {@link SetAssignFelhasznaloTermekResponse }
     * 
     */
    public SetAssignFelhasznaloTermekResponse createSetAssignFelhasznaloTermekResponse() {
        return new SetAssignFelhasznaloTermekResponse();
    }

    /**
     * Create an instance of {@link SetMoveUpFotermek }
     * 
     */
    public SetMoveUpFotermek createSetMoveUpFotermek() {
        return new SetMoveUpFotermek();
    }

    /**
     * Create an instance of {@link SetMoveDownFotermek }
     * 
     */
    public SetMoveDownFotermek createSetMoveDownFotermek() {
        return new SetMoveDownFotermek();
    }

    /**
     * Create an instance of {@link GetClientAppVersionResponse }
     * 
     */
    public GetClientAppVersionResponse createGetClientAppVersionResponse() {
        return new GetClientAppVersionResponse();
    }

    /**
     * Create an instance of {@link GetSzamlaModosit }
     * 
     */
    public GetSzamlaModosit createGetSzamlaModosit() {
        return new GetSzamlaModosit();
    }

    /**
     * Create an instance of {@link SetTmpDelSzamlaTetel }
     * 
     */
    public SetTmpDelSzamlaTetel createSetTmpDelSzamlaTetel() {
        return new SetTmpDelSzamlaTetel();
    }

    /**
     * Create an instance of {@link GetFelhasznaloTermekekResponse }
     * 
     */
    public GetFelhasznaloTermekekResponse createGetFelhasznaloTermekekResponse() {
        return new GetFelhasznaloTermekekResponse();
    }

    /**
     * Create an instance of {@link GetSpeedButtonTermekListResponse }
     * 
     */
    public GetSpeedButtonTermekListResponse createGetSpeedButtonTermekListResponse() {
        return new GetSpeedButtonTermekListResponse();
    }

    /**
     * Create an instance of {@link GetInitNyugtaForm }
     * 
     */
    public GetInitNyugtaForm createGetInitNyugtaForm() {
        return new GetInitNyugtaForm();
    }

    /**
     * Create an instance of {@link GetFelhasznaloTermekekSzla }
     * 
     */
    public GetFelhasznaloTermekekSzla createGetFelhasznaloTermekekSzla() {
        return new GetFelhasznaloTermekekSzla();
    }

    /**
     * Create an instance of {@link SetSafeInvoiceAndPrintByWsfId }
     * 
     */
    public SetSafeInvoiceAndPrintByWsfId createSetSafeInvoiceAndPrintByWsfId() {
        return new SetSafeInvoiceAndPrintByWsfId();
    }

    /**
     * Create an instance of {@link GetSafeSzamlaFejList }
     * 
     */
    public GetSafeSzamlaFejList createGetSafeSzamlaFejList() {
        return new GetSafeSzamlaFejList();
    }

    /**
     * Create an instance of {@link SetMoveUpFelhasznaloTermekResponse }
     * 
     */
    public SetMoveUpFelhasznaloTermekResponse createSetMoveUpFelhasznaloTermekResponse() {
        return new SetMoveUpFelhasznaloTermekResponse();
    }

    /**
     * Create an instance of {@link SetSzamla }
     * 
     */
    public SetSzamla createSetSzamla() {
        return new SetSzamla();
    }

    /**
     * Create an instance of {@link SetSafePrintInvoice }
     * 
     */
    public SetSafePrintInvoice createSetSafePrintInvoice() {
        return new SetSafePrintInvoice();
    }

    /**
     * Create an instance of {@link SetMoveUpFelhasznaloTermek }
     * 
     */
    public SetMoveUpFelhasznaloTermek createSetMoveUpFelhasznaloTermek() {
        return new SetMoveUpFelhasznaloTermek();
    }

    /**
     * Create an instance of {@link GetTmpSzamlaTetelList }
     * 
     */
    public GetTmpSzamlaTetelList createGetTmpSzamlaTetelList() {
        return new GetTmpSzamlaTetelList();
    }

    /**
     * Create an instance of {@link RdsTryLoginResponse }
     * 
     */
    public RdsTryLoginResponse createRdsTryLoginResponse() {
        return new RdsTryLoginResponse();
    }

    /**
     * Create an instance of {@link TryLoginResult }
     * 
     */
    public TryLoginResult createTryLoginResult() {
        return new TryLoginResult();
    }

    /**
     * Create an instance of {@link GetSzamlaTetelListResponse }
     * 
     */
    public GetSzamlaTetelListResponse createGetSzamlaTetelListResponse() {
        return new GetSzamlaTetelListResponse();
    }

    /**
     * Create an instance of {@link GetMainPartnerResponse }
     * 
     */
    public GetMainPartnerResponse createGetMainPartnerResponse() {
        return new GetMainPartnerResponse();
    }

    /**
     * Create an instance of {@link GetTermekList }
     * 
     */
    public GetTermekList createGetTermekList() {
        return new GetTermekList();
    }

    /**
     * Create an instance of {@link SetSafeStornoInvoiceAndPrint }
     * 
     */
    public SetSafeStornoInvoiceAndPrint createSetSafeStornoInvoiceAndPrint() {
        return new SetSafeStornoInvoiceAndPrint();
    }

    /**
     * Create an instance of {@link GetNyelvekListResponse }
     * 
     */
    public GetNyelvekListResponse createGetNyelvekListResponse() {
        return new GetNyelvekListResponse();
    }

    /**
     * Create an instance of {@link SetPartnerkDel }
     * 
     */
    public SetPartnerkDel createSetPartnerkDel() {
        return new SetPartnerkDel();
    }

    /**
     * Create an instance of {@link SetPartner }
     * 
     */
    public SetPartner createSetPartner() {
        return new SetPartner();
    }

    /**
     * Create an instance of {@link GetSzamlaModositResponse }
     * 
     */
    public GetSzamlaModositResponse createGetSzamlaModositResponse() {
        return new GetSzamlaModositResponse();
    }

    /**
     * Create an instance of {@link WetSzamlaModositResult }
     * 
     */
    public WetSzamlaModositResult createWetSzamlaModositResult() {
        return new WetSzamlaModositResult();
    }

    /**
     * Create an instance of {@link SetFelhasznaloResponse }
     * 
     */
    public SetFelhasznaloResponse createSetFelhasznaloResponse() {
        return new SetFelhasznaloResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRdsNaplo }
     * 
     */
    public ArrayOfRdsNaplo createArrayOfRdsNaplo() {
        return new ArrayOfRdsNaplo();
    }

    /**
     * Create an instance of {@link WetUrlapEllenorzesElem }
     * 
     */
    public WetUrlapEllenorzesElem createWetUrlapEllenorzesElem() {
        return new WetUrlapEllenorzesElem();
    }

    /**
     * Create an instance of {@link ArrayOfRdsFelhasznalok }
     * 
     */
    public ArrayOfRdsFelhasznalok createArrayOfRdsFelhasznalok() {
        return new ArrayOfRdsFelhasznalok();
    }

    /**
     * Create an instance of {@link ArrayOfWetSafeSzamlaFej }
     * 
     */
    public ArrayOfWetSafeSzamlaFej createArrayOfWetSafeSzamlaFej() {
        return new ArrayOfWetSafeSzamlaFej();
    }

    /**
     * Create an instance of {@link WetSafeSzamlaFej }
     * 
     */
    public WetSafeSzamlaFej createWetSafeSzamlaFej() {
        return new WetSafeSzamlaFej();
    }

    /**
     * Create an instance of {@link WetAdatszotarElem }
     * 
     */
    public WetAdatszotarElem createWetAdatszotarElem() {
        return new WetAdatszotarElem();
    }

    /**
     * Create an instance of {@link RdsSzerepkorok }
     * 
     */
    public RdsSzerepkorok createRdsSzerepkorok() {
        return new RdsSzerepkorok();
    }

    /**
     * Create an instance of {@link WetFoTermek }
     * 
     */
    public WetFoTermek createWetFoTermek() {
        return new WetFoTermek();
    }

    /**
     * Create an instance of {@link ArrayOfWetSzamlaFej }
     * 
     */
    public ArrayOfWetSzamlaFej createArrayOfWetSzamlaFej() {
        return new ArrayOfWetSzamlaFej();
    }

    /**
     * Create an instance of {@link ArrayOfRdsPartnerek }
     * 
     */
    public ArrayOfRdsPartnerek createArrayOfRdsPartnerek() {
        return new ArrayOfRdsPartnerek();
    }

    /**
     * Create an instance of {@link ArrayOfRdsSzerepkorok }
     * 
     */
    public ArrayOfRdsSzerepkorok createArrayOfRdsSzerepkorok() {
        return new ArrayOfRdsSzerepkorok();
    }

    /**
     * Create an instance of {@link ArrayOfWetAdatszotarElem }
     * 
     */
    public ArrayOfWetAdatszotarElem createArrayOfWetAdatszotarElem() {
        return new ArrayOfWetAdatszotarElem();
    }

    /**
     * Create an instance of {@link ArrayOfWetSzamlaTetel }
     * 
     */
    public ArrayOfWetSzamlaTetel createArrayOfWetSzamlaTetel() {
        return new ArrayOfWetSzamlaTetel();
    }

    /**
     * Create an instance of {@link ArrayOfWetTermek }
     * 
     */
    public ArrayOfWetTermek createArrayOfWetTermek() {
        return new ArrayOfWetTermek();
    }

    /**
     * Create an instance of {@link ArrayOfWetUrlapEllenorzesElem }
     * 
     */
    public ArrayOfWetUrlapEllenorzesElem createArrayOfWetUrlapEllenorzesElem() {
        return new ArrayOfWetUrlapEllenorzesElem();
    }

    /**
     * Create an instance of {@link ArrayOfWetFoTermek }
     * 
     */
    public ArrayOfWetFoTermek createArrayOfWetFoTermek() {
        return new ArrayOfWetFoTermek();
    }

    /**
     * Create an instance of {@link RdsNaplo }
     * 
     */
    public RdsNaplo createRdsNaplo() {
        return new RdsNaplo();
    }

}
