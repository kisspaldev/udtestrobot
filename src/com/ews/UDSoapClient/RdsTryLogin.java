
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdpAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felJelszo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felNyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rdpAzon",
    "felLogin",
    "felJelszo",
    "felNyelv"
})
@XmlRootElement(name = "rdsTryLogin")
public class RdsTryLogin {

    protected String rdpAzon;
    protected String felLogin;
    protected String felJelszo;
    protected String felNyelv;

    /**
     * Gets the value of the rdpAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAzon() {
        return rdpAzon;
    }

    /**
     * Sets the value of the rdpAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAzon(String value) {
        this.rdpAzon = value;
    }

    /**
     * Gets the value of the felLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelLogin() {
        return felLogin;
    }

    /**
     * Sets the value of the felLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelLogin(String value) {
        this.felLogin = value;
    }

    /**
     * Gets the value of the felJelszo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelJelszo() {
        return felJelszo;
    }

    /**
     * Sets the value of the felJelszo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelJelszo(String value) {
        this.felJelszo = value;
    }

    /**
     * Gets the value of the felNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelNyelv() {
        return felNyelv;
    }

    /**
     * Sets the value of the felNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelNyelv(String value) {
        this.felNyelv = value;
    }

}
