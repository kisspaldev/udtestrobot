
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSafeBillInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeBillInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsf_rendszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_nyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_hivszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_formatum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeBillInput", propOrder = {
    "wsfRendszam",
    "wsfNyelv",
    "wsfHivszam",
    "wsfFormatum"
})
public class WetSafeBillInput {

    @XmlElement(name = "wsf_rendszam")
    protected String wsfRendszam;
    @XmlElement(name = "wsf_nyelv")
    protected String wsfNyelv;
    @XmlElement(name = "wsf_hivszam")
    protected String wsfHivszam;
    @XmlElement(name = "wsf_formatum")
    protected String wsfFormatum;

    /**
     * Gets the value of the wsfRendszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfRendszam() {
        return wsfRendszam;
    }

    /**
     * Sets the value of the wsfRendszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfRendszam(String value) {
        this.wsfRendszam = value;
    }

    /**
     * Gets the value of the wsfNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfNyelv() {
        return wsfNyelv;
    }

    /**
     * Sets the value of the wsfNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfNyelv(String value) {
        this.wsfNyelv = value;
    }

    /**
     * Gets the value of the wsfHivszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfHivszam() {
        return wsfHivszam;
    }

    /**
     * Sets the value of the wsfHivszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfHivszam(String value) {
        this.wsfHivszam = value;
    }

    /**
     * Gets the value of the wsfFormatum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfFormatum() {
        return wsfFormatum;
    }

    /**
     * Sets the value of the wsfFormatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfFormatum(String value) {
        this.wsfFormatum = value;
    }

}
