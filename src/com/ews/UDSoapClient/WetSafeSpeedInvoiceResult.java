
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSafeSpeedInvoiceResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeSpeedInvoiceResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="webUrlapEllenorzesList" type="{http://tempuri.org/}ArrayOfWetUrlapEllenorzesElem" minOccurs="0"/>
 *         &lt;element name="webSzamlaFej" type="{http://tempuri.org/}WetSzamlaFej" minOccurs="0"/>
 *         &lt;element name="wetSzamlaTetelList" type="{http://tempuri.org/}ArrayOfWetSzamlaTetel" minOccurs="0"/>
 *         &lt;element name="wsfId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsfAzonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="peldanyszam" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeSpeedInvoiceResult", propOrder = {
    "webUrlapEllenorzesList",
    "webSzamlaFej",
    "wetSzamlaTetelList",
    "wsfId",
    "wsfAzonosito",
    "peldanyszam",
    "status"
})
public class WetSafeSpeedInvoiceResult {

    protected ArrayOfWetUrlapEllenorzesElem webUrlapEllenorzesList;
    protected WetSzamlaFej webSzamlaFej;
    protected ArrayOfWetSzamlaTetel wetSzamlaTetelList;
    protected int wsfId;
    protected String wsfAzonosito;
    protected int peldanyszam;
    protected String status;

    /**
     * Gets the value of the webUrlapEllenorzesList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetUrlapEllenorzesElem }
     *     
     */
    public ArrayOfWetUrlapEllenorzesElem getWebUrlapEllenorzesList() {
        return webUrlapEllenorzesList;
    }

    /**
     * Sets the value of the webUrlapEllenorzesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetUrlapEllenorzesElem }
     *     
     */
    public void setWebUrlapEllenorzesList(ArrayOfWetUrlapEllenorzesElem value) {
        this.webUrlapEllenorzesList = value;
    }

    /**
     * Gets the value of the webSzamlaFej property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaFej }
     *     
     */
    public WetSzamlaFej getWebSzamlaFej() {
        return webSzamlaFej;
    }

    /**
     * Sets the value of the webSzamlaFej property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaFej }
     *     
     */
    public void setWebSzamlaFej(WetSzamlaFej value) {
        this.webSzamlaFej = value;
    }

    /**
     * Gets the value of the wetSzamlaTetelList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public ArrayOfWetSzamlaTetel getWetSzamlaTetelList() {
        return wetSzamlaTetelList;
    }

    /**
     * Sets the value of the wetSzamlaTetelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public void setWetSzamlaTetelList(ArrayOfWetSzamlaTetel value) {
        this.wetSzamlaTetelList = value;
    }

    /**
     * Gets the value of the wsfId property.
     * 
     */
    public int getWsfId() {
        return wsfId;
    }

    /**
     * Sets the value of the wsfId property.
     * 
     */
    public void setWsfId(int value) {
        this.wsfId = value;
    }

    /**
     * Gets the value of the wsfAzonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfAzonosito() {
        return wsfAzonosito;
    }

    /**
     * Sets the value of the wsfAzonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfAzonosito(String value) {
        this.wsfAzonosito = value;
    }

    /**
     * Gets the value of the peldanyszam property.
     * 
     */
    public int getPeldanyszam() {
        return peldanyszam;
    }

    /**
     * Sets the value of the peldanyszam property.
     * 
     */
    public void setPeldanyszam(int value) {
        this.peldanyszam = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
