
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RdsSzerepkorok complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RdsSzerepkorok">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rszId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rszNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rszAktiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RdsSzerepkorok", propOrder = {
    "rszId",
    "rszNev",
    "rszAktiv"
})
public class RdsSzerepkorok {

    protected String rszId;
    protected String rszNev;
    protected String rszAktiv;

    /**
     * Gets the value of the rszId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRszId() {
        return rszId;
    }

    /**
     * Sets the value of the rszId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRszId(String value) {
        this.rszId = value;
    }

    /**
     * Gets the value of the rszNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRszNev() {
        return rszNev;
    }

    /**
     * Sets the value of the rszNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRszNev(String value) {
        this.rszNev = value;
    }

    /**
     * Gets the value of the rszAktiv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRszAktiv() {
        return rszAktiv;
    }

    /**
     * Sets the value of the rszAktiv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRszAktiv(String value) {
        this.rszAktiv = value;
    }

}
