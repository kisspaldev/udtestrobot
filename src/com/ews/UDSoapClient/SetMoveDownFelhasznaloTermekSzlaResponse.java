
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setMoveDownFelhasznaloTermekSzlaResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setMoveDownFelhasznaloTermekSzlaResult"
})
@XmlRootElement(name = "setMoveDownFelhasznaloTermekSzlaResponse")
public class SetMoveDownFelhasznaloTermekSzlaResponse {

    protected String setMoveDownFelhasznaloTermekSzlaResult;

    /**
     * Gets the value of the setMoveDownFelhasznaloTermekSzlaResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetMoveDownFelhasznaloTermekSzlaResult() {
        return setMoveDownFelhasznaloTermekSzlaResult;
    }

    /**
     * Sets the value of the setMoveDownFelhasznaloTermekSzlaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetMoveDownFelhasznaloTermekSzlaResult(String value) {
        this.setMoveDownFelhasznaloTermekSzlaResult = value;
    }

}
