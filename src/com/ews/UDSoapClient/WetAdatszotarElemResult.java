
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetAdatszotarElemResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetAdatszotarElemResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetAdatszotarElemList" type="{http://tempuri.org/}ArrayOfWetAdatszotarElem" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetAdatszotarElemResult", propOrder = {
    "wetAdatszotarElemList",
    "status"
})
public class WetAdatszotarElemResult {

    protected ArrayOfWetAdatszotarElem wetAdatszotarElemList;
    protected String status;

    /**
     * Gets the value of the wetAdatszotarElemList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public ArrayOfWetAdatszotarElem getWetAdatszotarElemList() {
        return wetAdatszotarElemList;
    }

    /**
     * Sets the value of the wetAdatszotarElemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public void setWetAdatszotarElemList(ArrayOfWetAdatszotarElem value) {
        this.wetAdatszotarElemList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
