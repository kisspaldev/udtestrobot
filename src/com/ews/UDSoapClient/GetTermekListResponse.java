
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTermekListResult" type="{http://tempuri.org/}wetTermekekListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTermekListResult"
})
@XmlRootElement(name = "getTermekListResponse")
public class GetTermekListResponse {

    protected WetTermekekListResult getTermekListResult;

    /**
     * Gets the value of the getTermekListResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetTermekekListResult }
     *     
     */
    public WetTermekekListResult getGetTermekListResult() {
        return getTermekListResult;
    }

    /**
     * Sets the value of the getTermekListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetTermekekListResult }
     *     
     */
    public void setGetTermekListResult(WetTermekekListResult value) {
        this.getTermekListResult = value;
    }

}
