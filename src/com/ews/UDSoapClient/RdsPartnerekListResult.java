
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rdsPartnerekListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rdsPartnerekListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdsPartnerekList" type="{http://tempuri.org/}ArrayOfRdsPartnerek" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rdsPartnerekListResult", propOrder = {
    "rdsPartnerekList",
    "status"
})
public class RdsPartnerekListResult {

    protected ArrayOfRdsPartnerek rdsPartnerekList;
    protected String status;

    /**
     * Gets the value of the rdsPartnerekList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRdsPartnerek }
     *     
     */
    public ArrayOfRdsPartnerek getRdsPartnerekList() {
        return rdsPartnerekList;
    }

    /**
     * Sets the value of the rdsPartnerekList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRdsPartnerek }
     *     
     */
    public void setRdsPartnerekList(ArrayOfRdsPartnerek value) {
        this.rdsPartnerekList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
