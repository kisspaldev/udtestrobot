
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RdsFelhasznalok complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RdsFelhasznalok">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="felId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="felLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felJelszo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felAktiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felRszid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rszNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felRdpid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felNyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felNyelv_ertek" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RdsFelhasznalok", propOrder = {
    "felId",
    "felLogin",
    "felJelszo",
    "felNev",
    "felAktiv",
    "felRszid",
    "rszNev",
    "felRdpid",
    "rdpNev",
    "rdpAzon",
    "felNyelv",
    "felNyelvErtek"
})
public class RdsFelhasznalok {

    protected int felId;
    protected String felLogin;
    protected String felJelszo;
    protected String felNev;
    protected String felAktiv;
    protected String felRszid;
    protected String rszNev;
    protected int felRdpid;
    protected String rdpNev;
    protected String rdpAzon;
    protected String felNyelv;
    @XmlElement(name = "felNyelv_ertek")
    protected String felNyelvErtek;

    /**
     * Gets the value of the felId property.
     * 
     */
    public int getFelId() {
        return felId;
    }

    /**
     * Sets the value of the felId property.
     * 
     */
    public void setFelId(int value) {
        this.felId = value;
    }

    /**
     * Gets the value of the felLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelLogin() {
        return felLogin;
    }

    /**
     * Sets the value of the felLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelLogin(String value) {
        this.felLogin = value;
    }

    /**
     * Gets the value of the felJelszo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelJelszo() {
        return felJelszo;
    }

    /**
     * Sets the value of the felJelszo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelJelszo(String value) {
        this.felJelszo = value;
    }

    /**
     * Gets the value of the felNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelNev() {
        return felNev;
    }

    /**
     * Sets the value of the felNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelNev(String value) {
        this.felNev = value;
    }

    /**
     * Gets the value of the felAktiv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelAktiv() {
        return felAktiv;
    }

    /**
     * Sets the value of the felAktiv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelAktiv(String value) {
        this.felAktiv = value;
    }

    /**
     * Gets the value of the felRszid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelRszid() {
        return felRszid;
    }

    /**
     * Sets the value of the felRszid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelRszid(String value) {
        this.felRszid = value;
    }

    /**
     * Gets the value of the rszNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRszNev() {
        return rszNev;
    }

    /**
     * Sets the value of the rszNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRszNev(String value) {
        this.rszNev = value;
    }

    /**
     * Gets the value of the felRdpid property.
     * 
     */
    public int getFelRdpid() {
        return felRdpid;
    }

    /**
     * Sets the value of the felRdpid property.
     * 
     */
    public void setFelRdpid(int value) {
        this.felRdpid = value;
    }

    /**
     * Gets the value of the rdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpNev() {
        return rdpNev;
    }

    /**
     * Sets the value of the rdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpNev(String value) {
        this.rdpNev = value;
    }

    /**
     * Gets the value of the rdpAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAzon() {
        return rdpAzon;
    }

    /**
     * Sets the value of the rdpAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAzon(String value) {
        this.rdpAzon = value;
    }

    /**
     * Gets the value of the felNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelNyelv() {
        return felNyelv;
    }

    /**
     * Sets the value of the felNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelNyelv(String value) {
        this.felNyelv = value;
    }

    /**
     * Gets the value of the felNyelvErtek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelNyelvErtek() {
        return felNyelvErtek;
    }

    /**
     * Sets the value of the felNyelvErtek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelNyelvErtek(String value) {
        this.felNyelvErtek = value;
    }

}
