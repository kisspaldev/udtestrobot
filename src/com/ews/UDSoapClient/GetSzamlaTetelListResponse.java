
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSzamlaTetelListResult" type="{http://tempuri.org/}wetSzamlaTetelListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSzamlaTetelListResult"
})
@XmlRootElement(name = "getSzamlaTetelListResponse")
public class GetSzamlaTetelListResponse {

    protected WetSzamlaTetelListResult getSzamlaTetelListResult;

    /**
     * Gets the value of the getSzamlaTetelListResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaTetelListResult }
     *     
     */
    public WetSzamlaTetelListResult getGetSzamlaTetelListResult() {
        return getSzamlaTetelListResult;
    }

    /**
     * Sets the value of the getSzamlaTetelListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaTetelListResult }
     *     
     */
    public void setGetSzamlaTetelListResult(WetSzamlaTetelListResult value) {
        this.getSzamlaTetelListResult = value;
    }

}
