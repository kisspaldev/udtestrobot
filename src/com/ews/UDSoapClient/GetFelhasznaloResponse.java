
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getFelhasznaloResult" type="{http://tempuri.org/}RdsFelhasznalok" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFelhasznaloResult"
})
@XmlRootElement(name = "getFelhasznaloResponse")
public class GetFelhasznaloResponse {

    protected RdsFelhasznalok getFelhasznaloResult;

    /**
     * Gets the value of the getFelhasznaloResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsFelhasznalok }
     *     
     */
    public RdsFelhasznalok getGetFelhasznaloResult() {
        return getFelhasznaloResult;
    }

    /**
     * Sets the value of the getFelhasznaloResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsFelhasznalok }
     *     
     */
    public void setGetFelhasznaloResult(RdsFelhasznalok value) {
        this.getFelhasznaloResult = value;
    }

}
