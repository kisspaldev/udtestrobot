
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetSzamlaFejListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetSzamlaFejListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetSzamlaFejList" type="{http://tempuri.org/}ArrayOfWetSzamlaFej" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetSzamlaFejListResult", propOrder = {
    "wetSzamlaFejList",
    "status"
})
public class WetSzamlaFejListResult {

    protected ArrayOfWetSzamlaFej wetSzamlaFejList;
    protected String status;

    /**
     * Gets the value of the wetSzamlaFejList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetSzamlaFej }
     *     
     */
    public ArrayOfWetSzamlaFej getWetSzamlaFejList() {
        return wetSzamlaFejList;
    }

    /**
     * Sets the value of the wetSzamlaFejList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetSzamlaFej }
     *     
     */
    public void setWetSzamlaFejList(ArrayOfWetSzamlaFej value) {
        this.wetSzamlaFejList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
