
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WetSafeSzamlaFej complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeSzamlaFej">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsf_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_azonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_teljesitve" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_kelte" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_fizhat" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_v_rdpnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdp_ertekesito_neve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_sum_brutto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_sum_netto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_sum_afa" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_fizmod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_stornozva" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_orig_wfsazonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_rendszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_rogzitve" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeSzamlaFej", propOrder = {
    "wsfId",
    "wsfAzonosito",
    "wsfTeljesitve",
    "wsfKelte",
    "wsfFizhat",
    "wsfVRdpnev",
    "wsfERdpErtekesitoNeve",
    "wsfSumBrutto",
    "wsfSumNetto",
    "wsfSumAfa",
    "wsfFizmod",
    "wsfStornozva",
    "wsfOrigWfsazonosito",
    "wsfRendszam",
    "wsfRogzitve"
})
public class WetSafeSzamlaFej {

    @XmlElement(name = "wsf_id")
    protected int wsfId;
    @XmlElement(name = "wsf_azonosito")
    protected String wsfAzonosito;
    @XmlElement(name = "wsf_teljesitve", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfTeljesitve;
    @XmlElement(name = "wsf_kelte", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfKelte;
    @XmlElement(name = "wsf_fizhat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfFizhat;
    @XmlElement(name = "wsf_v_rdpnev")
    protected String wsfVRdpnev;
    @XmlElement(name = "wsf_e_rdp_ertekesito_neve")
    protected String wsfERdpErtekesitoNeve;
    @XmlElement(name = "wsf_sum_brutto")
    protected double wsfSumBrutto;
    @XmlElement(name = "wsf_sum_netto")
    protected double wsfSumNetto;
    @XmlElement(name = "wsf_sum_afa")
    protected double wsfSumAfa;
    @XmlElement(name = "wsf_fizmod")
    protected String wsfFizmod;
    @XmlElement(name = "wsf_stornozva", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfStornozva;
    @XmlElement(name = "wsf_orig_wfsazonosito")
    protected String wsfOrigWfsazonosito;
    @XmlElement(name = "wsf_rendszam")
    protected String wsfRendszam;
    @XmlElement(name = "wsf_rogzitve", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfRogzitve;

    /**
     * Gets the value of the wsfId property.
     * 
     */
    public int getWsfId() {
        return wsfId;
    }

    /**
     * Sets the value of the wsfId property.
     * 
     */
    public void setWsfId(int value) {
        this.wsfId = value;
    }

    /**
     * Gets the value of the wsfAzonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfAzonosito() {
        return wsfAzonosito;
    }

    /**
     * Sets the value of the wsfAzonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfAzonosito(String value) {
        this.wsfAzonosito = value;
    }

    /**
     * Gets the value of the wsfTeljesitve property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfTeljesitve() {
        return wsfTeljesitve;
    }

    /**
     * Sets the value of the wsfTeljesitve property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfTeljesitve(XMLGregorianCalendar value) {
        this.wsfTeljesitve = value;
    }

    /**
     * Gets the value of the wsfKelte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfKelte() {
        return wsfKelte;
    }

    /**
     * Sets the value of the wsfKelte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfKelte(XMLGregorianCalendar value) {
        this.wsfKelte = value;
    }

    /**
     * Gets the value of the wsfFizhat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfFizhat() {
        return wsfFizhat;
    }

    /**
     * Sets the value of the wsfFizhat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfFizhat(XMLGregorianCalendar value) {
        this.wsfFizhat = value;
    }

    /**
     * Gets the value of the wsfVRdpnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpnev() {
        return wsfVRdpnev;
    }

    /**
     * Sets the value of the wsfVRdpnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpnev(String value) {
        this.wsfVRdpnev = value;
    }

    /**
     * Gets the value of the wsfERdpErtekesitoNeve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpErtekesitoNeve() {
        return wsfERdpErtekesitoNeve;
    }

    /**
     * Sets the value of the wsfERdpErtekesitoNeve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpErtekesitoNeve(String value) {
        this.wsfERdpErtekesitoNeve = value;
    }

    /**
     * Gets the value of the wsfSumBrutto property.
     * 
     */
    public double getWsfSumBrutto() {
        return wsfSumBrutto;
    }

    /**
     * Sets the value of the wsfSumBrutto property.
     * 
     */
    public void setWsfSumBrutto(double value) {
        this.wsfSumBrutto = value;
    }

    /**
     * Gets the value of the wsfSumNetto property.
     * 
     */
    public double getWsfSumNetto() {
        return wsfSumNetto;
    }

    /**
     * Sets the value of the wsfSumNetto property.
     * 
     */
    public void setWsfSumNetto(double value) {
        this.wsfSumNetto = value;
    }

    /**
     * Gets the value of the wsfSumAfa property.
     * 
     */
    public double getWsfSumAfa() {
        return wsfSumAfa;
    }

    /**
     * Sets the value of the wsfSumAfa property.
     * 
     */
    public void setWsfSumAfa(double value) {
        this.wsfSumAfa = value;
    }

    /**
     * Gets the value of the wsfFizmod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfFizmod() {
        return wsfFizmod;
    }

    /**
     * Sets the value of the wsfFizmod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfFizmod(String value) {
        this.wsfFizmod = value;
    }

    /**
     * Gets the value of the wsfStornozva property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfStornozva() {
        return wsfStornozva;
    }

    /**
     * Sets the value of the wsfStornozva property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfStornozva(XMLGregorianCalendar value) {
        this.wsfStornozva = value;
    }

    /**
     * Gets the value of the wsfOrigWfsazonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfOrigWfsazonosito() {
        return wsfOrigWfsazonosito;
    }

    /**
     * Sets the value of the wsfOrigWfsazonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfOrigWfsazonosito(String value) {
        this.wsfOrigWfsazonosito = value;
    }

    /**
     * Gets the value of the wsfRendszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfRendszam() {
        return wsfRendszam;
    }

    /**
     * Sets the value of the wsfRendszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfRendszam(String value) {
        this.wsfRendszam = value;
    }

    /**
     * Gets the value of the wsfRogzitve property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfRogzitve() {
        return wsfRogzitve;
    }

    /**
     * Sets the value of the wsfRogzitve property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfRogzitve(XMLGregorianCalendar value) {
        this.wsfRogzitve = value;
    }

}
