
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetAdatszotarElem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetAdatszotarElem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetAdatszotarElem" type="{http://tempuri.org/}wetAdatszotarElem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetAdatszotarElem", propOrder = {
    "wetAdatszotarElem"
})
public class ArrayOfWetAdatszotarElem {

    @XmlElement(nillable = true)
    protected List<WetAdatszotarElem> wetAdatszotarElem;

    /**
     * Gets the value of the wetAdatszotarElem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetAdatszotarElem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetAdatszotarElem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetAdatszotarElem }
     * 
     * 
     */
    public List<WetAdatszotarElem> getWetAdatszotarElem() {
        if (wetAdatszotarElem == null) {
            wetAdatszotarElem = new ArrayList<WetAdatszotarElem>();
        }
        return this.wetAdatszotarElem;
    }

}
