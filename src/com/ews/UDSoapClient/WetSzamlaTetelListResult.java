
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetSzamlaTetelListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetSzamlaTetelListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetSzamlaTetelList" type="{http://tempuri.org/}ArrayOfWetSzamlaTetel" minOccurs="0"/>
 *         &lt;element name="wsfSumNetto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsfSumAfa" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsfSumBrutto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetSzamlaTetelListResult", propOrder = {
    "wetSzamlaTetelList",
    "wsfSumNetto",
    "wsfSumAfa",
    "wsfSumBrutto",
    "status"
})
public class WetSzamlaTetelListResult {

    protected ArrayOfWetSzamlaTetel wetSzamlaTetelList;
    protected double wsfSumNetto;
    protected double wsfSumAfa;
    protected double wsfSumBrutto;
    protected String status;

    /**
     * Gets the value of the wetSzamlaTetelList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public ArrayOfWetSzamlaTetel getWetSzamlaTetelList() {
        return wetSzamlaTetelList;
    }

    /**
     * Sets the value of the wetSzamlaTetelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public void setWetSzamlaTetelList(ArrayOfWetSzamlaTetel value) {
        this.wetSzamlaTetelList = value;
    }

    /**
     * Gets the value of the wsfSumNetto property.
     * 
     */
    public double getWsfSumNetto() {
        return wsfSumNetto;
    }

    /**
     * Sets the value of the wsfSumNetto property.
     * 
     */
    public void setWsfSumNetto(double value) {
        this.wsfSumNetto = value;
    }

    /**
     * Gets the value of the wsfSumAfa property.
     * 
     */
    public double getWsfSumAfa() {
        return wsfSumAfa;
    }

    /**
     * Sets the value of the wsfSumAfa property.
     * 
     */
    public void setWsfSumAfa(double value) {
        this.wsfSumAfa = value;
    }

    /**
     * Gets the value of the wsfSumBrutto property.
     * 
     */
    public double getWsfSumBrutto() {
        return wsfSumBrutto;
    }

    /**
     * Sets the value of the wsfSumBrutto property.
     * 
     */
    public void setWsfSumBrutto(double value) {
        this.wsfSumBrutto = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
