
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetSzamlaUrlapResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetSzamlaUrlapResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetAdatszotarElemNyelvekList" type="{http://tempuri.org/}ArrayOfWetAdatszotarElem" minOccurs="0"/>
 *         &lt;element name="wetAdatszotarElemFizmodokList" type="{http://tempuri.org/}ArrayOfWetAdatszotarElem" minOccurs="0"/>
 *         &lt;element name="wetAdatszotarElemOrszagokList" type="{http://tempuri.org/}ArrayOfWetAdatszotarElem" minOccurs="0"/>
 *         &lt;element name="rdsPartnerekList" type="{http://tempuri.org/}ArrayOfRdsPartnerek" minOccurs="0"/>
 *         &lt;element name="wetTermekekList" type="{http://tempuri.org/}ArrayOfWetTermek" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetSzamlaUrlapResult", propOrder = {
    "status",
    "wetAdatszotarElemNyelvekList",
    "wetAdatszotarElemFizmodokList",
    "wetAdatszotarElemOrszagokList",
    "rdsPartnerekList",
    "wetTermekekList"
})
public class WetSzamlaUrlapResult {

    protected String status;
    protected ArrayOfWetAdatszotarElem wetAdatszotarElemNyelvekList;
    protected ArrayOfWetAdatszotarElem wetAdatszotarElemFizmodokList;
    protected ArrayOfWetAdatszotarElem wetAdatszotarElemOrszagokList;
    protected ArrayOfRdsPartnerek rdsPartnerekList;
    protected ArrayOfWetTermek wetTermekekList;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the wetAdatszotarElemNyelvekList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public ArrayOfWetAdatszotarElem getWetAdatszotarElemNyelvekList() {
        return wetAdatszotarElemNyelvekList;
    }

    /**
     * Sets the value of the wetAdatszotarElemNyelvekList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public void setWetAdatszotarElemNyelvekList(ArrayOfWetAdatszotarElem value) {
        this.wetAdatszotarElemNyelvekList = value;
    }

    /**
     * Gets the value of the wetAdatszotarElemFizmodokList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public ArrayOfWetAdatszotarElem getWetAdatszotarElemFizmodokList() {
        return wetAdatszotarElemFizmodokList;
    }

    /**
     * Sets the value of the wetAdatszotarElemFizmodokList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public void setWetAdatszotarElemFizmodokList(ArrayOfWetAdatszotarElem value) {
        this.wetAdatszotarElemFizmodokList = value;
    }

    /**
     * Gets the value of the wetAdatszotarElemOrszagokList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public ArrayOfWetAdatszotarElem getWetAdatszotarElemOrszagokList() {
        return wetAdatszotarElemOrszagokList;
    }

    /**
     * Sets the value of the wetAdatszotarElemOrszagokList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetAdatszotarElem }
     *     
     */
    public void setWetAdatszotarElemOrszagokList(ArrayOfWetAdatszotarElem value) {
        this.wetAdatszotarElemOrszagokList = value;
    }

    /**
     * Gets the value of the rdsPartnerekList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRdsPartnerek }
     *     
     */
    public ArrayOfRdsPartnerek getRdsPartnerekList() {
        return rdsPartnerekList;
    }

    /**
     * Sets the value of the rdsPartnerekList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRdsPartnerek }
     *     
     */
    public void setRdsPartnerekList(ArrayOfRdsPartnerek value) {
        this.rdsPartnerekList = value;
    }

    /**
     * Gets the value of the wetTermekekList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public ArrayOfWetTermek getWetTermekekList() {
        return wetTermekekList;
    }

    /**
     * Sets the value of the wetTermekekList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public void setWetTermekekList(ArrayOfWetTermek value) {
        this.wetTermekekList = value;
    }

}
