
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rdsFelhasznaloTermekListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rdsFelhasznaloTermekListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wtfTermekek" type="{http://tempuri.org/}ArrayOfWetTermek" minOccurs="0"/>
 *         &lt;element name="termekek" type="{http://tempuri.org/}ArrayOfWetTermek" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rdsFelhasznaloTermekListResult", propOrder = {
    "wtfTermekek",
    "termekek",
    "status"
})
public class RdsFelhasznaloTermekListResult {

    protected ArrayOfWetTermek wtfTermekek;
    protected ArrayOfWetTermek termekek;
    protected String status;

    /**
     * Gets the value of the wtfTermekek property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public ArrayOfWetTermek getWtfTermekek() {
        return wtfTermekek;
    }

    /**
     * Sets the value of the wtfTermekek property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public void setWtfTermekek(ArrayOfWetTermek value) {
        this.wtfTermekek = value;
    }

    /**
     * Gets the value of the termekek property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public ArrayOfWetTermek getTermekek() {
        return termekek;
    }

    /**
     * Sets the value of the termekek property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public void setTermekek(ArrayOfWetTermek value) {
        this.termekek = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
