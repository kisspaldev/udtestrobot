
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSafeSpeedInvoiceInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeSpeedInvoiceInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsf_v_rdpnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpeuadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpbankszamla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdporszag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpiranyitoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpvaros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdputcahazszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_rendszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_sum_brutto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wst_wetid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_nyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeSpeedInvoiceInput", propOrder = {
    "wsfVRdpnev",
    "wsfVRdpadoszam",
    "wsfVRdpeuadoszam",
    "wsfVRdpbankszamla",
    "wsfVRdporszag",
    "wsfVRdpiranyitoszam",
    "wsfVRdpvaros",
    "wsfVRdputcahazszam",
    "wsfRendszam",
    "wsfSumBrutto",
    "wstWetid",
    "wsfNyelv"
})
public class WetSafeSpeedInvoiceInput {

    @XmlElement(name = "wsf_v_rdpnev")
    protected String wsfVRdpnev;
    @XmlElement(name = "wsf_v_rdpadoszam")
    protected String wsfVRdpadoszam;
    @XmlElement(name = "wsf_v_rdpeuadoszam")
    protected String wsfVRdpeuadoszam;
    @XmlElement(name = "wsf_v_rdpbankszamla")
    protected String wsfVRdpbankszamla;
    @XmlElement(name = "wsf_v_rdporszag")
    protected String wsfVRdporszag;
    @XmlElement(name = "wsf_v_rdpiranyitoszam")
    protected String wsfVRdpiranyitoszam;
    @XmlElement(name = "wsf_v_rdpvaros")
    protected String wsfVRdpvaros;
    @XmlElement(name = "wsf_v_rdputcahazszam")
    protected String wsfVRdputcahazszam;
    @XmlElement(name = "wsf_rendszam")
    protected String wsfRendszam;
    @XmlElement(name = "wsf_sum_brutto")
    protected double wsfSumBrutto;
    @XmlElement(name = "wst_wetid")
    protected int wstWetid;
    @XmlElement(name = "wsf_nyelv")
    protected String wsfNyelv;

    /**
     * Gets the value of the wsfVRdpnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpnev() {
        return wsfVRdpnev;
    }

    /**
     * Sets the value of the wsfVRdpnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpnev(String value) {
        this.wsfVRdpnev = value;
    }

    /**
     * Gets the value of the wsfVRdpadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpadoszam() {
        return wsfVRdpadoszam;
    }

    /**
     * Sets the value of the wsfVRdpadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpadoszam(String value) {
        this.wsfVRdpadoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpeuadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpeuadoszam() {
        return wsfVRdpeuadoszam;
    }

    /**
     * Sets the value of the wsfVRdpeuadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpeuadoszam(String value) {
        this.wsfVRdpeuadoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpbankszamla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpbankszamla() {
        return wsfVRdpbankszamla;
    }

    /**
     * Sets the value of the wsfVRdpbankszamla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpbankszamla(String value) {
        this.wsfVRdpbankszamla = value;
    }

    /**
     * Gets the value of the wsfVRdporszag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdporszag() {
        return wsfVRdporszag;
    }

    /**
     * Sets the value of the wsfVRdporszag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdporszag(String value) {
        this.wsfVRdporszag = value;
    }

    /**
     * Gets the value of the wsfVRdpiranyitoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpiranyitoszam() {
        return wsfVRdpiranyitoszam;
    }

    /**
     * Sets the value of the wsfVRdpiranyitoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpiranyitoszam(String value) {
        this.wsfVRdpiranyitoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpvaros property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpvaros() {
        return wsfVRdpvaros;
    }

    /**
     * Sets the value of the wsfVRdpvaros property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpvaros(String value) {
        this.wsfVRdpvaros = value;
    }

    /**
     * Gets the value of the wsfVRdputcahazszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdputcahazszam() {
        return wsfVRdputcahazszam;
    }

    /**
     * Sets the value of the wsfVRdputcahazszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdputcahazszam(String value) {
        this.wsfVRdputcahazszam = value;
    }

    /**
     * Gets the value of the wsfRendszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfRendszam() {
        return wsfRendszam;
    }

    /**
     * Sets the value of the wsfRendszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfRendszam(String value) {
        this.wsfRendszam = value;
    }

    /**
     * Gets the value of the wsfSumBrutto property.
     * 
     */
    public double getWsfSumBrutto() {
        return wsfSumBrutto;
    }

    /**
     * Sets the value of the wsfSumBrutto property.
     * 
     */
    public void setWsfSumBrutto(double value) {
        this.wsfSumBrutto = value;
    }

    /**
     * Gets the value of the wstWetid property.
     * 
     */
    public int getWstWetid() {
        return wstWetid;
    }

    /**
     * Sets the value of the wstWetid property.
     * 
     */
    public void setWstWetid(int value) {
        this.wstWetid = value;
    }

    /**
     * Gets the value of the wsfNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfNyelv() {
        return wsfNyelv;
    }

    /**
     * Sets the value of the wsfNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfNyelv(String value) {
        this.wsfNyelv = value;
    }

}
