
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRdsNaplo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRdsNaplo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RdsNaplo" type="{http://tempuri.org/}RdsNaplo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRdsNaplo", propOrder = {
    "rdsNaplo"
})
public class ArrayOfRdsNaplo {

    @XmlElement(name = "RdsNaplo", nillable = true)
    protected List<RdsNaplo> rdsNaplo;

    /**
     * Gets the value of the rdsNaplo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rdsNaplo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRdsNaplo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RdsNaplo }
     * 
     * 
     */
    public List<RdsNaplo> getRdsNaplo() {
        if (rdsNaplo == null) {
            rdsNaplo = new ArrayList<RdsNaplo>();
        }
        return this.rdsNaplo;
    }

}
