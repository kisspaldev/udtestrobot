
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetSzamlaModositResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetSzamlaModositResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetSzamlaFej" type="{http://tempuri.org/}WetSzamlaFej" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetSzamlaModositResult", propOrder = {
    "wetSzamlaFej",
    "status"
})
public class WetSzamlaModositResult {

    protected WetSzamlaFej wetSzamlaFej;
    protected String status;

    /**
     * Gets the value of the wetSzamlaFej property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaFej }
     *     
     */
    public WetSzamlaFej getWetSzamlaFej() {
        return wetSzamlaFej;
    }

    /**
     * Sets the value of the wetSzamlaFej property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaFej }
     *     
     */
    public void setWetSzamlaFej(WetSzamlaFej value) {
        this.wetSzamlaFej = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
