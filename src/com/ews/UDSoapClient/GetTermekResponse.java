
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTermekResult" type="{http://tempuri.org/}WetTermek" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTermekResult"
})
@XmlRootElement(name = "getTermekResponse")
public class GetTermekResponse {

    protected WetTermek getTermekResult;

    /**
     * Gets the value of the getTermekResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetTermek }
     *     
     */
    public WetTermek getGetTermekResult() {
        return getTermekResult;
    }

    /**
     * Sets the value of the getTermekResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetTermek }
     *     
     */
    public void setGetTermekResult(WetTermek value) {
        this.getTermekResult = value;
    }

}
