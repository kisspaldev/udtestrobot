
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getInitNyugtaFormResult" type="{http://tempuri.org/}wetNyugtaUrlapResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getInitNyugtaFormResult"
})
@XmlRootElement(name = "getInitNyugtaFormResponse")
public class GetInitNyugtaFormResponse {

    protected WetNyugtaUrlapResult getInitNyugtaFormResult;

    /**
     * Gets the value of the getInitNyugtaFormResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetNyugtaUrlapResult }
     *     
     */
    public WetNyugtaUrlapResult getGetInitNyugtaFormResult() {
        return getInitNyugtaFormResult;
    }

    /**
     * Sets the value of the getInitNyugtaFormResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetNyugtaUrlapResult }
     *     
     */
    public void setGetInitNyugtaFormResult(WetNyugtaUrlapResult value) {
        this.getInitNyugtaFormResult = value;
    }

}
