
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPartnerListResult" type="{http://tempuri.org/}rdsPartnerekListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPartnerListResult"
})
@XmlRootElement(name = "getPartnerListResponse")
public class GetPartnerListResponse {

    protected RdsPartnerekListResult getPartnerListResult;

    /**
     * Gets the value of the getPartnerListResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsPartnerekListResult }
     *     
     */
    public RdsPartnerekListResult getGetPartnerListResult() {
        return getPartnerListResult;
    }

    /**
     * Sets the value of the getPartnerListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsPartnerekListResult }
     *     
     */
    public void setGetPartnerListResult(RdsPartnerekListResult value) {
        this.getPartnerListResult = value;
    }

}
