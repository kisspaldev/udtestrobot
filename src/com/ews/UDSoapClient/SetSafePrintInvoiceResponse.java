
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSafePrintInvoiceResult" type="{http://tempuri.org/}WetSafeInvoiceDetailResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSafePrintInvoiceResult"
})
@XmlRootElement(name = "setSafePrintInvoiceResponse")
public class SetSafePrintInvoiceResponse {

    protected WetSafeInvoiceDetailResult setSafePrintInvoiceResult;

    /**
     * Gets the value of the setSafePrintInvoiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeInvoiceDetailResult }
     *     
     */
    public WetSafeInvoiceDetailResult getSetSafePrintInvoiceResult() {
        return setSafePrintInvoiceResult;
    }

    /**
     * Sets the value of the setSafePrintInvoiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeInvoiceDetailResult }
     *     
     */
    public void setSetSafePrintInvoiceResult(WetSafeInvoiceDetailResult value) {
        this.setSafePrintInvoiceResult = value;
    }

}
