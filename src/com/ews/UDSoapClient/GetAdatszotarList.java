
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRadTipus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRadNyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pRadTipus",
    "pRadNyelv"
})
@XmlRootElement(name = "getAdatszotarList")
public class GetAdatszotarList {

    protected String pSid;
    protected String pRadTipus;
    protected String pRadNyelv;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pRadTipus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRadTipus() {
        return pRadTipus;
    }

    /**
     * Sets the value of the pRadTipus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRadTipus(String value) {
        this.pRadTipus = value;
    }

    /**
     * Gets the value of the pRadNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRadNyelv() {
        return pRadNyelv;
    }

    /**
     * Sets the value of the pRadNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRadNyelv(String value) {
        this.pRadNyelv = value;
    }

}
