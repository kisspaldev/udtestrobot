
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pWetSzamlaFej" type="{http://tempuri.org/}WetSzamlaFej" minOccurs="0"/>
 *         &lt;element name="pFilRdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pWetSzamlaFej",
    "pFilRdpId"
})
@XmlRootElement(name = "setSzamla")
public class SetSzamla {

    protected String pSid;
    protected WetSzamlaFej pWetSzamlaFej;
    protected int pFilRdpId;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pWetSzamlaFej property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaFej }
     *     
     */
    public WetSzamlaFej getPWetSzamlaFej() {
        return pWetSzamlaFej;
    }

    /**
     * Sets the value of the pWetSzamlaFej property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaFej }
     *     
     */
    public void setPWetSzamlaFej(WetSzamlaFej value) {
        this.pWetSzamlaFej = value;
    }

    /**
     * Gets the value of the pFilRdpId property.
     * 
     */
    public int getPFilRdpId() {
        return pFilRdpId;
    }

    /**
     * Sets the value of the pFilRdpId property.
     * 
     */
    public void setPFilRdpId(int value) {
        this.pFilRdpId = value;
    }

}
