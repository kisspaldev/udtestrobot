
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getMainPartnerResult" type="{http://tempuri.org/}RdsPartnerek" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMainPartnerResult"
})
@XmlRootElement(name = "getMainPartnerResponse")
public class GetMainPartnerResponse {

    protected RdsPartnerek getMainPartnerResult;

    /**
     * Gets the value of the getMainPartnerResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsPartnerek }
     *     
     */
    public RdsPartnerek getGetMainPartnerResult() {
        return getMainPartnerResult;
    }

    /**
     * Sets the value of the getMainPartnerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsPartnerek }
     *     
     */
    public void setGetMainPartnerResult(RdsPartnerek value) {
        this.getMainPartnerResult = value;
    }

}
