
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFelJelenlegiJelszo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFelJelszo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFelJelszoMegerositese" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pFelJelenlegiJelszo",
    "pFelJelszo",
    "pFelJelszoMegerositese"
})
@XmlRootElement(name = "setSajatJelszo")
public class SetSajatJelszo {

    protected String pSid;
    protected String pFelJelenlegiJelszo;
    protected String pFelJelszo;
    protected String pFelJelszoMegerositese;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pFelJelenlegiJelszo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFelJelenlegiJelszo() {
        return pFelJelenlegiJelszo;
    }

    /**
     * Sets the value of the pFelJelenlegiJelszo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFelJelenlegiJelszo(String value) {
        this.pFelJelenlegiJelszo = value;
    }

    /**
     * Gets the value of the pFelJelszo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFelJelszo() {
        return pFelJelszo;
    }

    /**
     * Sets the value of the pFelJelszo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFelJelszo(String value) {
        this.pFelJelszo = value;
    }

    /**
     * Gets the value of the pFelJelszoMegerositese property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFelJelszoMegerositese() {
        return pFelJelszoMegerositese;
    }

    /**
     * Sets the value of the pFelJelszoMegerositese property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFelJelszoMegerositese(String value) {
        this.pFelJelszoMegerositese = value;
    }

}
