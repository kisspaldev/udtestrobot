
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getNaploListResult" type="{http://tempuri.org/}rdsNaploListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNaploListResult"
})
@XmlRootElement(name = "getNaploListResponse")
public class GetNaploListResponse {

    protected RdsNaploListResult getNaploListResult;

    /**
     * Gets the value of the getNaploListResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsNaploListResult }
     *     
     */
    public RdsNaploListResult getGetNaploListResult() {
        return getNaploListResult;
    }

    /**
     * Sets the value of the getNaploListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsNaploListResult }
     *     
     */
    public void setGetNaploListResult(RdsNaploListResult value) {
        this.getNaploListResult = value;
    }

}
