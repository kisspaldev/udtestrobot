
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setTmpSzamlaTetelResult" type="{http://tempuri.org/}webUrlapEllenorzesResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setTmpSzamlaTetelResult"
})
@XmlRootElement(name = "setTmpSzamlaTetelResponse")
public class SetTmpSzamlaTetelResponse {

    protected WebUrlapEllenorzesResult setTmpSzamlaTetelResult;

    /**
     * Gets the value of the setTmpSzamlaTetelResult property.
     * 
     * @return
     *     possible object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public WebUrlapEllenorzesResult getSetTmpSzamlaTetelResult() {
        return setTmpSzamlaTetelResult;
    }

    /**
     * Sets the value of the setTmpSzamlaTetelResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public void setSetTmpSzamlaTetelResult(WebUrlapEllenorzesResult value) {
        this.setTmpSzamlaTetelResult = value;
    }

}
