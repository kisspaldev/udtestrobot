
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetSafeSzamlaFejListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetSafeSzamlaFejListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetSzamlaFejList" type="{http://tempuri.org/}ArrayOfWetSafeSzamlaFej" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetSafeSzamlaFejListResult", propOrder = {
    "wetSzamlaFejList",
    "status"
})
public class WetSafeSzamlaFejListResult {

    protected ArrayOfWetSafeSzamlaFej wetSzamlaFejList;
    protected String status;

    /**
     * Gets the value of the wetSzamlaFejList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetSafeSzamlaFej }
     *     
     */
    public ArrayOfWetSafeSzamlaFej getWetSzamlaFejList() {
        return wetSzamlaFejList;
    }

    /**
     * Sets the value of the wetSzamlaFejList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetSafeSzamlaFej }
     *     
     */
    public void setWetSzamlaFejList(ArrayOfWetSafeSzamlaFej value) {
        this.wetSzamlaFejList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
