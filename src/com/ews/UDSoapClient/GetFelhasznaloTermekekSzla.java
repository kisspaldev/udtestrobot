
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pLang"
})
@XmlRootElement(name = "getFelhasznaloTermekekSzla")
public class GetFelhasznaloTermekekSzla {

    protected String pSid;
    protected String pLang;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pLang property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLang() {
        return pLang;
    }

    /**
     * Sets the value of the pLang property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLang(String value) {
        this.pLang = value;
    }

}
