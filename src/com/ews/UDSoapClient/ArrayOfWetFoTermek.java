
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetFoTermek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetFoTermek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WetFoTermek" type="{http://tempuri.org/}WetFoTermek" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetFoTermek", propOrder = {
    "wetFoTermek"
})
public class ArrayOfWetFoTermek {

    @XmlElement(name = "WetFoTermek", nillable = true)
    protected List<WetFoTermek> wetFoTermek;

    /**
     * Gets the value of the wetFoTermek property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetFoTermek property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetFoTermek().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetFoTermek }
     * 
     * 
     */
    public List<WetFoTermek> getWetFoTermek() {
        if (wetFoTermek == null) {
            wetFoTermek = new ArrayList<WetFoTermek>();
        }
        return this.wetFoTermek;
    }

}
