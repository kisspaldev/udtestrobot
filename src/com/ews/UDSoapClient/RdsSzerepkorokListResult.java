
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rdsSzerepkorokListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rdsSzerepkorokListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdsSzerepkorokList" type="{http://tempuri.org/}ArrayOfRdsSzerepkorok" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rdsSzerepkorokListResult", propOrder = {
    "rdsSzerepkorokList",
    "status"
})
public class RdsSzerepkorokListResult {

    protected ArrayOfRdsSzerepkorok rdsSzerepkorokList;
    protected String status;

    /**
     * Gets the value of the rdsSzerepkorokList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRdsSzerepkorok }
     *     
     */
    public ArrayOfRdsSzerepkorok getRdsSzerepkorokList() {
        return rdsSzerepkorokList;
    }

    /**
     * Sets the value of the rdsSzerepkorokList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRdsSzerepkorok }
     *     
     */
    public void setRdsSzerepkorokList(ArrayOfRdsSzerepkorok value) {
        this.rdsSzerepkorokList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
