
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetFoTermek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetFoTermek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wffOrder" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wetlNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wff_tipus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetFoTermek", propOrder = {
    "wetId",
    "wffOrder",
    "wetlNev",
    "wffTipus"
})
public class WetFoTermek {

    protected int wetId;
    protected int wffOrder;
    protected String wetlNev;
    @XmlElement(name = "wff_tipus")
    protected String wffTipus;

    /**
     * Gets the value of the wetId property.
     * 
     */
    public int getWetId() {
        return wetId;
    }

    /**
     * Sets the value of the wetId property.
     * 
     */
    public void setWetId(int value) {
        this.wetId = value;
    }

    /**
     * Gets the value of the wffOrder property.
     * 
     */
    public int getWffOrder() {
        return wffOrder;
    }

    /**
     * Sets the value of the wffOrder property.
     * 
     */
    public void setWffOrder(int value) {
        this.wffOrder = value;
    }

    /**
     * Gets the value of the wetlNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetlNev() {
        return wetlNev;
    }

    /**
     * Sets the value of the wetlNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetlNev(String value) {
        this.wetlNev = value;
    }

    /**
     * Gets the value of the wffTipus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWffTipus() {
        return wffTipus;
    }

    /**
     * Sets the value of the wffTipus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWffTipus(String value) {
        this.wffTipus = value;
    }

}
