
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetTermek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetTermek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wetAzonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetlNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetSzjvtsz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetAfakulcs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetAfakulcsValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetAfakulcsNumValue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wetMeegys" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetMeegysValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetNettoEgysar" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wetlNyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetlNyelvValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetSbutton" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wetSbuttonEgysar" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wetIconFileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="termekTipus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetTermek", propOrder = {
    "wetId",
    "wetAzonosito",
    "wetlNev",
    "wetSzjvtsz",
    "wetAfakulcs",
    "wetAfakulcsValue",
    "wetAfakulcsNumValue",
    "wetMeegys",
    "wetMeegysValue",
    "wetNettoEgysar",
    "wetlNyelv",
    "wetlNyelvValue",
    "wetSbutton",
    "wetSbuttonEgysar",
    "wetIconFileName",
    "termekTipus"
})
public class WetTermek {

    protected int wetId;
    protected String wetAzonosito;
    protected String wetlNev;
    protected String wetSzjvtsz;
    protected String wetAfakulcs;
    protected String wetAfakulcsValue;
    protected double wetAfakulcsNumValue;
    protected String wetMeegys;
    protected String wetMeegysValue;
    protected double wetNettoEgysar;
    protected String wetlNyelv;
    protected String wetlNyelvValue;
    protected String wetSbutton;
    protected double wetSbuttonEgysar;
    protected String wetIconFileName;
    protected String termekTipus;

    /**
     * Gets the value of the wetId property.
     * 
     */
    public int getWetId() {
        return wetId;
    }

    /**
     * Sets the value of the wetId property.
     * 
     */
    public void setWetId(int value) {
        this.wetId = value;
    }

    /**
     * Gets the value of the wetAzonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetAzonosito() {
        return wetAzonosito;
    }

    /**
     * Sets the value of the wetAzonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetAzonosito(String value) {
        this.wetAzonosito = value;
    }

    /**
     * Gets the value of the wetlNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetlNev() {
        return wetlNev;
    }

    /**
     * Sets the value of the wetlNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetlNev(String value) {
        this.wetlNev = value;
    }

    /**
     * Gets the value of the wetSzjvtsz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetSzjvtsz() {
        return wetSzjvtsz;
    }

    /**
     * Sets the value of the wetSzjvtsz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetSzjvtsz(String value) {
        this.wetSzjvtsz = value;
    }

    /**
     * Gets the value of the wetAfakulcs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetAfakulcs() {
        return wetAfakulcs;
    }

    /**
     * Sets the value of the wetAfakulcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetAfakulcs(String value) {
        this.wetAfakulcs = value;
    }

    /**
     * Gets the value of the wetAfakulcsValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetAfakulcsValue() {
        return wetAfakulcsValue;
    }

    /**
     * Sets the value of the wetAfakulcsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetAfakulcsValue(String value) {
        this.wetAfakulcsValue = value;
    }

    /**
     * Gets the value of the wetAfakulcsNumValue property.
     * 
     */
    public double getWetAfakulcsNumValue() {
        return wetAfakulcsNumValue;
    }

    /**
     * Sets the value of the wetAfakulcsNumValue property.
     * 
     */
    public void setWetAfakulcsNumValue(double value) {
        this.wetAfakulcsNumValue = value;
    }

    /**
     * Gets the value of the wetMeegys property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetMeegys() {
        return wetMeegys;
    }

    /**
     * Sets the value of the wetMeegys property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetMeegys(String value) {
        this.wetMeegys = value;
    }

    /**
     * Gets the value of the wetMeegysValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetMeegysValue() {
        return wetMeegysValue;
    }

    /**
     * Sets the value of the wetMeegysValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetMeegysValue(String value) {
        this.wetMeegysValue = value;
    }

    /**
     * Gets the value of the wetNettoEgysar property.
     * 
     */
    public double getWetNettoEgysar() {
        return wetNettoEgysar;
    }

    /**
     * Sets the value of the wetNettoEgysar property.
     * 
     */
    public void setWetNettoEgysar(double value) {
        this.wetNettoEgysar = value;
    }

    /**
     * Gets the value of the wetlNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetlNyelv() {
        return wetlNyelv;
    }

    /**
     * Sets the value of the wetlNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetlNyelv(String value) {
        this.wetlNyelv = value;
    }

    /**
     * Gets the value of the wetlNyelvValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetlNyelvValue() {
        return wetlNyelvValue;
    }

    /**
     * Sets the value of the wetlNyelvValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetlNyelvValue(String value) {
        this.wetlNyelvValue = value;
    }

    /**
     * Gets the value of the wetSbutton property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetSbutton() {
        return wetSbutton;
    }

    /**
     * Sets the value of the wetSbutton property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetSbutton(String value) {
        this.wetSbutton = value;
    }

    /**
     * Gets the value of the wetSbuttonEgysar property.
     * 
     */
    public double getWetSbuttonEgysar() {
        return wetSbuttonEgysar;
    }

    /**
     * Sets the value of the wetSbuttonEgysar property.
     * 
     */
    public void setWetSbuttonEgysar(double value) {
        this.wetSbuttonEgysar = value;
    }

    /**
     * Gets the value of the wetIconFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWetIconFileName() {
        return wetIconFileName;
    }

    /**
     * Sets the value of the wetIconFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWetIconFileName(String value) {
        this.wetIconFileName = value;
    }

    /**
     * Gets the value of the termekTipus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermekTipus() {
        return termekTipus;
    }

    /**
     * Sets the value of the termekTipus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermekTipus(String value) {
        this.termekTipus = value;
    }

}
