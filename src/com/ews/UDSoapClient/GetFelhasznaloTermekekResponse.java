
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getFelhasznaloTermekekResult" type="{http://tempuri.org/}rdsFelhasznaloTermekListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFelhasznaloTermekekResult"
})
@XmlRootElement(name = "getFelhasznaloTermekekResponse")
public class GetFelhasznaloTermekekResponse {

    protected RdsFelhasznaloTermekListResult getFelhasznaloTermekekResult;

    /**
     * Gets the value of the getFelhasznaloTermekekResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsFelhasznaloTermekListResult }
     *     
     */
    public RdsFelhasznaloTermekListResult getGetFelhasznaloTermekekResult() {
        return getFelhasznaloTermekekResult;
    }

    /**
     * Sets the value of the getFelhasznaloTermekekResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsFelhasznaloTermekListResult }
     *     
     */
    public void setGetFelhasznaloTermekekResult(RdsFelhasznaloTermekListResult value) {
        this.getFelhasznaloTermekekResult = value;
    }

}
