
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RdsNaplo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RdsNaplo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rsnDatum" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="rsnFunid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rsnFelid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rsnTipus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rsnTablaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rsnMegjegyzes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rsnTabla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rsnIp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdpAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="felNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="funNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RdsNaplo", propOrder = {
    "rsnDatum",
    "rsnFunid",
    "rsnFelid",
    "rsnTipus",
    "rsnTablaId",
    "rsnMegjegyzes",
    "rsnTabla",
    "rsnIp",
    "rdpNev",
    "rdpAzon",
    "felNev",
    "funNev"
})
public class RdsNaplo {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rsnDatum;
    protected String rsnFunid;
    protected int rsnFelid;
    protected String rsnTipus;
    protected int rsnTablaId;
    protected String rsnMegjegyzes;
    protected String rsnTabla;
    protected String rsnIp;
    protected String rdpNev;
    protected String rdpAzon;
    protected String felNev;
    protected String funNev;

    /**
     * Gets the value of the rsnDatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRsnDatum() {
        return rsnDatum;
    }

    /**
     * Sets the value of the rsnDatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRsnDatum(XMLGregorianCalendar value) {
        this.rsnDatum = value;
    }

    /**
     * Gets the value of the rsnFunid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsnFunid() {
        return rsnFunid;
    }

    /**
     * Sets the value of the rsnFunid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsnFunid(String value) {
        this.rsnFunid = value;
    }

    /**
     * Gets the value of the rsnFelid property.
     * 
     */
    public int getRsnFelid() {
        return rsnFelid;
    }

    /**
     * Sets the value of the rsnFelid property.
     * 
     */
    public void setRsnFelid(int value) {
        this.rsnFelid = value;
    }

    /**
     * Gets the value of the rsnTipus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsnTipus() {
        return rsnTipus;
    }

    /**
     * Sets the value of the rsnTipus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsnTipus(String value) {
        this.rsnTipus = value;
    }

    /**
     * Gets the value of the rsnTablaId property.
     * 
     */
    public int getRsnTablaId() {
        return rsnTablaId;
    }

    /**
     * Sets the value of the rsnTablaId property.
     * 
     */
    public void setRsnTablaId(int value) {
        this.rsnTablaId = value;
    }

    /**
     * Gets the value of the rsnMegjegyzes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsnMegjegyzes() {
        return rsnMegjegyzes;
    }

    /**
     * Sets the value of the rsnMegjegyzes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsnMegjegyzes(String value) {
        this.rsnMegjegyzes = value;
    }

    /**
     * Gets the value of the rsnTabla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsnTabla() {
        return rsnTabla;
    }

    /**
     * Sets the value of the rsnTabla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsnTabla(String value) {
        this.rsnTabla = value;
    }

    /**
     * Gets the value of the rsnIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsnIp() {
        return rsnIp;
    }

    /**
     * Sets the value of the rsnIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsnIp(String value) {
        this.rsnIp = value;
    }

    /**
     * Gets the value of the rdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpNev() {
        return rdpNev;
    }

    /**
     * Sets the value of the rdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpNev(String value) {
        this.rdpNev = value;
    }

    /**
     * Gets the value of the rdpAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpAzon() {
        return rdpAzon;
    }

    /**
     * Sets the value of the rdpAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpAzon(String value) {
        this.rdpAzon = value;
    }

    /**
     * Gets the value of the felNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFelNev() {
        return felNev;
    }

    /**
     * Sets the value of the felNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFelNev(String value) {
        this.felNev = value;
    }

    /**
     * Gets the value of the funNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunNev() {
        return funNev;
    }

    /**
     * Sets the value of the funNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunNev(String value) {
        this.funNev = value;
    }

}
