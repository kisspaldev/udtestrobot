
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSzamlaTetel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSzamlaTetel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wst_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wst_wsfid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wst_wetid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wst_wetazonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetszjvtsz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetafakulcs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetafakulcs_szamertek" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wst_wetafakulcs_ertek" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetmeegys" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetmeegys_ertek" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_wetnettoegysar" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wst_wetbruttoegysar" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wst_wetlnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wst_mennyiseg" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSzamlaTetel", propOrder = {
    "wstId",
    "wstWsfid",
    "wstWetid",
    "wstWetazonosito",
    "wstWetszjvtsz",
    "wstWetafakulcs",
    "wstWetafakulcsSzamertek",
    "wstWetafakulcsErtek",
    "wstWetmeegys",
    "wstWetmeegysErtek",
    "wstWetnettoegysar",
    "wstWetbruttoegysar",
    "wstWetlnev",
    "wstMennyiseg"
})
public class WetSzamlaTetel {

    @XmlElement(name = "wst_id")
    protected int wstId;
    @XmlElement(name = "wst_wsfid")
    protected int wstWsfid;
    @XmlElement(name = "wst_wetid")
    protected int wstWetid;
    @XmlElement(name = "wst_wetazonosito")
    protected String wstWetazonosito;
    @XmlElement(name = "wst_wetszjvtsz")
    protected String wstWetszjvtsz;
    @XmlElement(name = "wst_wetafakulcs")
    protected String wstWetafakulcs;
    @XmlElement(name = "wst_wetafakulcs_szamertek")
    protected double wstWetafakulcsSzamertek;
    @XmlElement(name = "wst_wetafakulcs_ertek")
    protected String wstWetafakulcsErtek;
    @XmlElement(name = "wst_wetmeegys")
    protected String wstWetmeegys;
    @XmlElement(name = "wst_wetmeegys_ertek")
    protected String wstWetmeegysErtek;
    @XmlElement(name = "wst_wetnettoegysar")
    protected double wstWetnettoegysar;
    @XmlElement(name = "wst_wetbruttoegysar")
    protected double wstWetbruttoegysar;
    @XmlElement(name = "wst_wetlnev")
    protected String wstWetlnev;
    @XmlElement(name = "wst_mennyiseg")
    protected double wstMennyiseg;

    /**
     * Gets the value of the wstId property.
     * 
     */
    public int getWstId() {
        return wstId;
    }

    /**
     * Sets the value of the wstId property.
     * 
     */
    public void setWstId(int value) {
        this.wstId = value;
    }

    /**
     * Gets the value of the wstWsfid property.
     * 
     */
    public int getWstWsfid() {
        return wstWsfid;
    }

    /**
     * Sets the value of the wstWsfid property.
     * 
     */
    public void setWstWsfid(int value) {
        this.wstWsfid = value;
    }

    /**
     * Gets the value of the wstWetid property.
     * 
     */
    public int getWstWetid() {
        return wstWetid;
    }

    /**
     * Sets the value of the wstWetid property.
     * 
     */
    public void setWstWetid(int value) {
        this.wstWetid = value;
    }

    /**
     * Gets the value of the wstWetazonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetazonosito() {
        return wstWetazonosito;
    }

    /**
     * Sets the value of the wstWetazonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetazonosito(String value) {
        this.wstWetazonosito = value;
    }

    /**
     * Gets the value of the wstWetszjvtsz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetszjvtsz() {
        return wstWetszjvtsz;
    }

    /**
     * Sets the value of the wstWetszjvtsz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetszjvtsz(String value) {
        this.wstWetszjvtsz = value;
    }

    /**
     * Gets the value of the wstWetafakulcs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetafakulcs() {
        return wstWetafakulcs;
    }

    /**
     * Sets the value of the wstWetafakulcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetafakulcs(String value) {
        this.wstWetafakulcs = value;
    }

    /**
     * Gets the value of the wstWetafakulcsSzamertek property.
     * 
     */
    public double getWstWetafakulcsSzamertek() {
        return wstWetafakulcsSzamertek;
    }

    /**
     * Sets the value of the wstWetafakulcsSzamertek property.
     * 
     */
    public void setWstWetafakulcsSzamertek(double value) {
        this.wstWetafakulcsSzamertek = value;
    }

    /**
     * Gets the value of the wstWetafakulcsErtek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetafakulcsErtek() {
        return wstWetafakulcsErtek;
    }

    /**
     * Sets the value of the wstWetafakulcsErtek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetafakulcsErtek(String value) {
        this.wstWetafakulcsErtek = value;
    }

    /**
     * Gets the value of the wstWetmeegys property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetmeegys() {
        return wstWetmeegys;
    }

    /**
     * Sets the value of the wstWetmeegys property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetmeegys(String value) {
        this.wstWetmeegys = value;
    }

    /**
     * Gets the value of the wstWetmeegysErtek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetmeegysErtek() {
        return wstWetmeegysErtek;
    }

    /**
     * Sets the value of the wstWetmeegysErtek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetmeegysErtek(String value) {
        this.wstWetmeegysErtek = value;
    }

    /**
     * Gets the value of the wstWetnettoegysar property.
     * 
     */
    public double getWstWetnettoegysar() {
        return wstWetnettoegysar;
    }

    /**
     * Sets the value of the wstWetnettoegysar property.
     * 
     */
    public void setWstWetnettoegysar(double value) {
        this.wstWetnettoegysar = value;
    }

    /**
     * Gets the value of the wstWetbruttoegysar property.
     * 
     */
    public double getWstWetbruttoegysar() {
        return wstWetbruttoegysar;
    }

    /**
     * Sets the value of the wstWetbruttoegysar property.
     * 
     */
    public void setWstWetbruttoegysar(double value) {
        this.wstWetbruttoegysar = value;
    }

    /**
     * Gets the value of the wstWetlnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWstWetlnev() {
        return wstWetlnev;
    }

    /**
     * Sets the value of the wstWetlnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWstWetlnev(String value) {
        this.wstWetlnev = value;
    }

    /**
     * Gets the value of the wstMennyiseg property.
     * 
     */
    public double getWstMennyiseg() {
        return wstMennyiseg;
    }

    /**
     * Sets the value of the wstMennyiseg property.
     * 
     */
    public void setWstMennyiseg(double value) {
        this.wstMennyiseg = value;
    }

}
