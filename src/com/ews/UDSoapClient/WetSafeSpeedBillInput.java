
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSafeSpeedBillInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeSpeedBillInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsf_rendszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_sum_brutto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wst_wetid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_nyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeSpeedBillInput", propOrder = {
    "wsfRendszam",
    "wsfSumBrutto",
    "wstWetid",
    "wsfNyelv"
})
public class WetSafeSpeedBillInput {

    @XmlElement(name = "wsf_rendszam")
    protected String wsfRendszam;
    @XmlElement(name = "wsf_sum_brutto")
    protected double wsfSumBrutto;
    @XmlElement(name = "wst_wetid")
    protected int wstWetid;
    @XmlElement(name = "wsf_nyelv")
    protected String wsfNyelv;

    /**
     * Gets the value of the wsfRendszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfRendszam() {
        return wsfRendszam;
    }

    /**
     * Sets the value of the wsfRendszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfRendszam(String value) {
        this.wsfRendszam = value;
    }

    /**
     * Gets the value of the wsfSumBrutto property.
     * 
     */
    public double getWsfSumBrutto() {
        return wsfSumBrutto;
    }

    /**
     * Sets the value of the wsfSumBrutto property.
     * 
     */
    public void setWsfSumBrutto(double value) {
        this.wsfSumBrutto = value;
    }

    /**
     * Gets the value of the wstWetid property.
     * 
     */
    public int getWstWetid() {
        return wstWetid;
    }

    /**
     * Sets the value of the wstWetid property.
     * 
     */
    public void setWstWetid(int value) {
        this.wstWetid = value;
    }

    /**
     * Gets the value of the wsfNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfNyelv() {
        return wsfNyelv;
    }

    /**
     * Sets the value of the wsfNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfNyelv(String value) {
        this.wsfNyelv = value;
    }

}
