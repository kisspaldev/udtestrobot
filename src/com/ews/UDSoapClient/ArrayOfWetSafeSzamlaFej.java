
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetSafeSzamlaFej complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetSafeSzamlaFej">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WetSafeSzamlaFej" type="{http://tempuri.org/}WetSafeSzamlaFej" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetSafeSzamlaFej", propOrder = {
    "wetSafeSzamlaFej"
})
public class ArrayOfWetSafeSzamlaFej {

    @XmlElement(name = "WetSafeSzamlaFej", nillable = true)
    protected List<WetSafeSzamlaFej> wetSafeSzamlaFej;

    /**
     * Gets the value of the wetSafeSzamlaFej property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetSafeSzamlaFej property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetSafeSzamlaFej().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetSafeSzamlaFej }
     * 
     * 
     */
    public List<WetSafeSzamlaFej> getWetSafeSzamlaFej() {
        if (wetSafeSzamlaFej == null) {
            wetSafeSzamlaFej = new ArrayList<WetSafeSzamlaFej>();
        }
        return this.wetSafeSzamlaFej;
    }

}
