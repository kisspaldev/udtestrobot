
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WetSzamlaFej complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSzamlaFej">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wsf_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_azonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_fizmod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_teljesitve" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_kelte" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_fizhat" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_sum_netto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_sum_afa" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_sum_brutto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="wsf_e_rdpnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpeuadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpbankszamla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdporszag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpiranyitoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpvaros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdputcahazszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_v_rdpnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpeuadoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpbankszamla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdporszag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpiranyitoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdpvaros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_v_rdputcahazszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdpid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_e_felid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_e_felnev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_megjegyzes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_nyelv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_penznem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_stornozva" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_orig_wsfid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_orig_wfsazonosito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_e_rdp_ertekesito_neve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_rendszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_hivszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdp_nev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdp_orszag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdp_iranyitoszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdp_varos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rdp_utcahazszam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_rogzitve" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="wsf_formatum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wsf_peldanyok" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsf_masolatok" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSzamlaFej", propOrder = {
    "wsfId",
    "wsfAzonosito",
    "wsfFizmod",
    "wsfTeljesitve",
    "wsfKelte",
    "wsfFizhat",
    "wsfSumNetto",
    "wsfSumAfa",
    "wsfSumBrutto",
    "wsfERdpnev",
    "wsfERdpadoszam",
    "wsfERdpeuadoszam",
    "wsfERdpbankszamla",
    "wsfERdporszag",
    "wsfERdpiranyitoszam",
    "wsfERdpvaros",
    "wsfERdputcahazszam",
    "wsfVRdpid",
    "wsfVRdpnev",
    "wsfVRdpadoszam",
    "wsfVRdpeuadoszam",
    "wsfVRdpbankszamla",
    "wsfVRdporszag",
    "wsfVRdpiranyitoszam",
    "wsfVRdpvaros",
    "wsfVRdputcahazszam",
    "wsfERdpid",
    "wsfEFelid",
    "wsfEFelnev",
    "wsfMegjegyzes",
    "wsfNyelv",
    "wsfPenznem",
    "wsfStornozva",
    "wsfOrigWsfid",
    "wsfOrigWfsazonosito",
    "wsfERdpErtekesitoNeve",
    "wsfRendszam",
    "wsfHivszam",
    "rdpNev",
    "rdpOrszag",
    "rdpIranyitoszam",
    "rdpVaros",
    "rdpUtcahazszam",
    "wsfRogzitve",
    "wsfFormatum",
    "wsfPeldanyok",
    "wsfMasolatok"
})
public class WetSzamlaFej {

    @XmlElement(name = "wsf_id")
    protected int wsfId;
    @XmlElement(name = "wsf_azonosito")
    protected String wsfAzonosito;
    @XmlElement(name = "wsf_fizmod")
    protected String wsfFizmod;
    @XmlElement(name = "wsf_teljesitve", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfTeljesitve;
    @XmlElement(name = "wsf_kelte", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfKelte;
    @XmlElement(name = "wsf_fizhat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfFizhat;
    @XmlElement(name = "wsf_sum_netto")
    protected double wsfSumNetto;
    @XmlElement(name = "wsf_sum_afa")
    protected double wsfSumAfa;
    @XmlElement(name = "wsf_sum_brutto")
    protected double wsfSumBrutto;
    @XmlElement(name = "wsf_e_rdpnev")
    protected String wsfERdpnev;
    @XmlElement(name = "wsf_e_rdpadoszam")
    protected String wsfERdpadoszam;
    @XmlElement(name = "wsf_e_rdpeuadoszam")
    protected String wsfERdpeuadoszam;
    @XmlElement(name = "wsf_e_rdpbankszamla")
    protected String wsfERdpbankszamla;
    @XmlElement(name = "wsf_e_rdporszag")
    protected String wsfERdporszag;
    @XmlElement(name = "wsf_e_rdpiranyitoszam")
    protected String wsfERdpiranyitoszam;
    @XmlElement(name = "wsf_e_rdpvaros")
    protected String wsfERdpvaros;
    @XmlElement(name = "wsf_e_rdputcahazszam")
    protected String wsfERdputcahazszam;
    @XmlElement(name = "wsf_v_rdpid")
    protected int wsfVRdpid;
    @XmlElement(name = "wsf_v_rdpnev")
    protected String wsfVRdpnev;
    @XmlElement(name = "wsf_v_rdpadoszam")
    protected String wsfVRdpadoszam;
    @XmlElement(name = "wsf_v_rdpeuadoszam")
    protected String wsfVRdpeuadoszam;
    @XmlElement(name = "wsf_v_rdpbankszamla")
    protected String wsfVRdpbankszamla;
    @XmlElement(name = "wsf_v_rdporszag")
    protected String wsfVRdporszag;
    @XmlElement(name = "wsf_v_rdpiranyitoszam")
    protected String wsfVRdpiranyitoszam;
    @XmlElement(name = "wsf_v_rdpvaros")
    protected String wsfVRdpvaros;
    @XmlElement(name = "wsf_v_rdputcahazszam")
    protected String wsfVRdputcahazszam;
    @XmlElement(name = "wsf_e_rdpid")
    protected int wsfERdpid;
    @XmlElement(name = "wsf_e_felid")
    protected int wsfEFelid;
    @XmlElement(name = "wsf_e_felnev")
    protected String wsfEFelnev;
    @XmlElement(name = "wsf_megjegyzes")
    protected String wsfMegjegyzes;
    @XmlElement(name = "wsf_nyelv")
    protected String wsfNyelv;
    @XmlElement(name = "wsf_penznem")
    protected String wsfPenznem;
    @XmlElement(name = "wsf_stornozva", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfStornozva;
    @XmlElement(name = "wsf_orig_wsfid")
    protected int wsfOrigWsfid;
    @XmlElement(name = "wsf_orig_wfsazonosito")
    protected String wsfOrigWfsazonosito;
    @XmlElement(name = "wsf_e_rdp_ertekesito_neve")
    protected String wsfERdpErtekesitoNeve;
    @XmlElement(name = "wsf_rendszam")
    protected String wsfRendszam;
    @XmlElement(name = "wsf_hivszam")
    protected String wsfHivszam;
    @XmlElement(name = "rdp_nev")
    protected String rdpNev;
    @XmlElement(name = "rdp_orszag")
    protected String rdpOrszag;
    @XmlElement(name = "rdp_iranyitoszam")
    protected String rdpIranyitoszam;
    @XmlElement(name = "rdp_varos")
    protected String rdpVaros;
    @XmlElement(name = "rdp_utcahazszam")
    protected String rdpUtcahazszam;
    @XmlElement(name = "wsf_rogzitve", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wsfRogzitve;
    @XmlElement(name = "wsf_formatum")
    protected String wsfFormatum;
    @XmlElement(name = "wsf_peldanyok")
    protected int wsfPeldanyok;
    @XmlElement(name = "wsf_masolatok")
    protected int wsfMasolatok;

    /**
     * Gets the value of the wsfId property.
     * 
     */
    public int getWsfId() {
        return wsfId;
    }

    /**
     * Sets the value of the wsfId property.
     * 
     */
    public void setWsfId(int value) {
        this.wsfId = value;
    }

    /**
     * Gets the value of the wsfAzonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfAzonosito() {
        return wsfAzonosito;
    }

    /**
     * Sets the value of the wsfAzonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfAzonosito(String value) {
        this.wsfAzonosito = value;
    }

    /**
     * Gets the value of the wsfFizmod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfFizmod() {
        return wsfFizmod;
    }

    /**
     * Sets the value of the wsfFizmod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfFizmod(String value) {
        this.wsfFizmod = value;
    }

    /**
     * Gets the value of the wsfTeljesitve property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfTeljesitve() {
        return wsfTeljesitve;
    }

    /**
     * Sets the value of the wsfTeljesitve property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfTeljesitve(XMLGregorianCalendar value) {
        this.wsfTeljesitve = value;
    }

    /**
     * Gets the value of the wsfKelte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfKelte() {
        return wsfKelte;
    }

    /**
     * Sets the value of the wsfKelte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfKelte(XMLGregorianCalendar value) {
        this.wsfKelte = value;
    }

    /**
     * Gets the value of the wsfFizhat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfFizhat() {
        return wsfFizhat;
    }

    /**
     * Sets the value of the wsfFizhat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfFizhat(XMLGregorianCalendar value) {
        this.wsfFizhat = value;
    }

    /**
     * Gets the value of the wsfSumNetto property.
     * 
     */
    public double getWsfSumNetto() {
        return wsfSumNetto;
    }

    /**
     * Sets the value of the wsfSumNetto property.
     * 
     */
    public void setWsfSumNetto(double value) {
        this.wsfSumNetto = value;
    }

    /**
     * Gets the value of the wsfSumAfa property.
     * 
     */
    public double getWsfSumAfa() {
        return wsfSumAfa;
    }

    /**
     * Sets the value of the wsfSumAfa property.
     * 
     */
    public void setWsfSumAfa(double value) {
        this.wsfSumAfa = value;
    }

    /**
     * Gets the value of the wsfSumBrutto property.
     * 
     */
    public double getWsfSumBrutto() {
        return wsfSumBrutto;
    }

    /**
     * Sets the value of the wsfSumBrutto property.
     * 
     */
    public void setWsfSumBrutto(double value) {
        this.wsfSumBrutto = value;
    }

    /**
     * Gets the value of the wsfERdpnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpnev() {
        return wsfERdpnev;
    }

    /**
     * Sets the value of the wsfERdpnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpnev(String value) {
        this.wsfERdpnev = value;
    }

    /**
     * Gets the value of the wsfERdpadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpadoszam() {
        return wsfERdpadoszam;
    }

    /**
     * Sets the value of the wsfERdpadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpadoszam(String value) {
        this.wsfERdpadoszam = value;
    }

    /**
     * Gets the value of the wsfERdpeuadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpeuadoszam() {
        return wsfERdpeuadoszam;
    }

    /**
     * Sets the value of the wsfERdpeuadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpeuadoszam(String value) {
        this.wsfERdpeuadoszam = value;
    }

    /**
     * Gets the value of the wsfERdpbankszamla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpbankszamla() {
        return wsfERdpbankszamla;
    }

    /**
     * Sets the value of the wsfERdpbankszamla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpbankszamla(String value) {
        this.wsfERdpbankszamla = value;
    }

    /**
     * Gets the value of the wsfERdporszag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdporszag() {
        return wsfERdporszag;
    }

    /**
     * Sets the value of the wsfERdporszag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdporszag(String value) {
        this.wsfERdporszag = value;
    }

    /**
     * Gets the value of the wsfERdpiranyitoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpiranyitoszam() {
        return wsfERdpiranyitoszam;
    }

    /**
     * Sets the value of the wsfERdpiranyitoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpiranyitoszam(String value) {
        this.wsfERdpiranyitoszam = value;
    }

    /**
     * Gets the value of the wsfERdpvaros property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpvaros() {
        return wsfERdpvaros;
    }

    /**
     * Sets the value of the wsfERdpvaros property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpvaros(String value) {
        this.wsfERdpvaros = value;
    }

    /**
     * Gets the value of the wsfERdputcahazszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdputcahazszam() {
        return wsfERdputcahazszam;
    }

    /**
     * Sets the value of the wsfERdputcahazszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdputcahazszam(String value) {
        this.wsfERdputcahazszam = value;
    }

    /**
     * Gets the value of the wsfVRdpid property.
     * 
     */
    public int getWsfVRdpid() {
        return wsfVRdpid;
    }

    /**
     * Sets the value of the wsfVRdpid property.
     * 
     */
    public void setWsfVRdpid(int value) {
        this.wsfVRdpid = value;
    }

    /**
     * Gets the value of the wsfVRdpnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpnev() {
        return wsfVRdpnev;
    }

    /**
     * Sets the value of the wsfVRdpnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpnev(String value) {
        this.wsfVRdpnev = value;
    }

    /**
     * Gets the value of the wsfVRdpadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpadoszam() {
        return wsfVRdpadoszam;
    }

    /**
     * Sets the value of the wsfVRdpadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpadoszam(String value) {
        this.wsfVRdpadoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpeuadoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpeuadoszam() {
        return wsfVRdpeuadoszam;
    }

    /**
     * Sets the value of the wsfVRdpeuadoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpeuadoszam(String value) {
        this.wsfVRdpeuadoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpbankszamla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpbankszamla() {
        return wsfVRdpbankszamla;
    }

    /**
     * Sets the value of the wsfVRdpbankszamla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpbankszamla(String value) {
        this.wsfVRdpbankszamla = value;
    }

    /**
     * Gets the value of the wsfVRdporszag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdporszag() {
        return wsfVRdporszag;
    }

    /**
     * Sets the value of the wsfVRdporszag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdporszag(String value) {
        this.wsfVRdporszag = value;
    }

    /**
     * Gets the value of the wsfVRdpiranyitoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpiranyitoszam() {
        return wsfVRdpiranyitoszam;
    }

    /**
     * Sets the value of the wsfVRdpiranyitoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpiranyitoszam(String value) {
        this.wsfVRdpiranyitoszam = value;
    }

    /**
     * Gets the value of the wsfVRdpvaros property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdpvaros() {
        return wsfVRdpvaros;
    }

    /**
     * Sets the value of the wsfVRdpvaros property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdpvaros(String value) {
        this.wsfVRdpvaros = value;
    }

    /**
     * Gets the value of the wsfVRdputcahazszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfVRdputcahazszam() {
        return wsfVRdputcahazszam;
    }

    /**
     * Sets the value of the wsfVRdputcahazszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfVRdputcahazszam(String value) {
        this.wsfVRdputcahazszam = value;
    }

    /**
     * Gets the value of the wsfERdpid property.
     * 
     */
    public int getWsfERdpid() {
        return wsfERdpid;
    }

    /**
     * Sets the value of the wsfERdpid property.
     * 
     */
    public void setWsfERdpid(int value) {
        this.wsfERdpid = value;
    }

    /**
     * Gets the value of the wsfEFelid property.
     * 
     */
    public int getWsfEFelid() {
        return wsfEFelid;
    }

    /**
     * Sets the value of the wsfEFelid property.
     * 
     */
    public void setWsfEFelid(int value) {
        this.wsfEFelid = value;
    }

    /**
     * Gets the value of the wsfEFelnev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfEFelnev() {
        return wsfEFelnev;
    }

    /**
     * Sets the value of the wsfEFelnev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfEFelnev(String value) {
        this.wsfEFelnev = value;
    }

    /**
     * Gets the value of the wsfMegjegyzes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfMegjegyzes() {
        return wsfMegjegyzes;
    }

    /**
     * Sets the value of the wsfMegjegyzes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfMegjegyzes(String value) {
        this.wsfMegjegyzes = value;
    }

    /**
     * Gets the value of the wsfNyelv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfNyelv() {
        return wsfNyelv;
    }

    /**
     * Sets the value of the wsfNyelv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfNyelv(String value) {
        this.wsfNyelv = value;
    }

    /**
     * Gets the value of the wsfPenznem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfPenznem() {
        return wsfPenznem;
    }

    /**
     * Sets the value of the wsfPenznem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfPenznem(String value) {
        this.wsfPenznem = value;
    }

    /**
     * Gets the value of the wsfStornozva property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfStornozva() {
        return wsfStornozva;
    }

    /**
     * Sets the value of the wsfStornozva property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfStornozva(XMLGregorianCalendar value) {
        this.wsfStornozva = value;
    }

    /**
     * Gets the value of the wsfOrigWsfid property.
     * 
     */
    public int getWsfOrigWsfid() {
        return wsfOrigWsfid;
    }

    /**
     * Sets the value of the wsfOrigWsfid property.
     * 
     */
    public void setWsfOrigWsfid(int value) {
        this.wsfOrigWsfid = value;
    }

    /**
     * Gets the value of the wsfOrigWfsazonosito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfOrigWfsazonosito() {
        return wsfOrigWfsazonosito;
    }

    /**
     * Sets the value of the wsfOrigWfsazonosito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfOrigWfsazonosito(String value) {
        this.wsfOrigWfsazonosito = value;
    }

    /**
     * Gets the value of the wsfERdpErtekesitoNeve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfERdpErtekesitoNeve() {
        return wsfERdpErtekesitoNeve;
    }

    /**
     * Sets the value of the wsfERdpErtekesitoNeve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfERdpErtekesitoNeve(String value) {
        this.wsfERdpErtekesitoNeve = value;
    }

    /**
     * Gets the value of the wsfRendszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfRendszam() {
        return wsfRendszam;
    }

    /**
     * Sets the value of the wsfRendszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfRendszam(String value) {
        this.wsfRendszam = value;
    }

    /**
     * Gets the value of the wsfHivszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfHivszam() {
        return wsfHivszam;
    }

    /**
     * Sets the value of the wsfHivszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfHivszam(String value) {
        this.wsfHivszam = value;
    }

    /**
     * Gets the value of the rdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpNev() {
        return rdpNev;
    }

    /**
     * Sets the value of the rdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpNev(String value) {
        this.rdpNev = value;
    }

    /**
     * Gets the value of the rdpOrszag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpOrszag() {
        return rdpOrszag;
    }

    /**
     * Sets the value of the rdpOrszag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpOrszag(String value) {
        this.rdpOrszag = value;
    }

    /**
     * Gets the value of the rdpIranyitoszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpIranyitoszam() {
        return rdpIranyitoszam;
    }

    /**
     * Sets the value of the rdpIranyitoszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpIranyitoszam(String value) {
        this.rdpIranyitoszam = value;
    }

    /**
     * Gets the value of the rdpVaros property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpVaros() {
        return rdpVaros;
    }

    /**
     * Sets the value of the rdpVaros property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpVaros(String value) {
        this.rdpVaros = value;
    }

    /**
     * Gets the value of the rdpUtcahazszam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRdpUtcahazszam() {
        return rdpUtcahazszam;
    }

    /**
     * Sets the value of the rdpUtcahazszam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRdpUtcahazszam(String value) {
        this.rdpUtcahazszam = value;
    }

    /**
     * Gets the value of the wsfRogzitve property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWsfRogzitve() {
        return wsfRogzitve;
    }

    /**
     * Sets the value of the wsfRogzitve property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWsfRogzitve(XMLGregorianCalendar value) {
        this.wsfRogzitve = value;
    }

    /**
     * Gets the value of the wsfFormatum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsfFormatum() {
        return wsfFormatum;
    }

    /**
     * Sets the value of the wsfFormatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsfFormatum(String value) {
        this.wsfFormatum = value;
    }

    /**
     * Gets the value of the wsfPeldanyok property.
     * 
     */
    public int getWsfPeldanyok() {
        return wsfPeldanyok;
    }

    /**
     * Sets the value of the wsfPeldanyok property.
     * 
     */
    public void setWsfPeldanyok(int value) {
        this.wsfPeldanyok = value;
    }

    /**
     * Gets the value of the wsfMasolatok property.
     * 
     */
    public int getWsfMasolatok() {
        return wsfMasolatok;
    }

    /**
     * Sets the value of the wsfMasolatok property.
     * 
     */
    public void setWsfMasolatok(int value) {
        this.wsfMasolatok = value;
    }

}
