
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rdsFelhasznaloFoTermekListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rdsFelhasznaloFoTermekListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wffTermekek" type="{http://tempuri.org/}ArrayOfWetFoTermek" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rdsFelhasznaloFoTermekListResult", propOrder = {
    "wffTermekek",
    "status"
})
public class RdsFelhasznaloFoTermekListResult {

    protected ArrayOfWetFoTermek wffTermekek;
    protected String status;

    /**
     * Gets the value of the wffTermekek property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetFoTermek }
     *     
     */
    public ArrayOfWetFoTermek getWffTermekek() {
        return wffTermekek;
    }

    /**
     * Sets the value of the wffTermekek property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetFoTermek }
     *     
     */
    public void setWffTermekek(ArrayOfWetFoTermek value) {
        this.wffTermekek = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
