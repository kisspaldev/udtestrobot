
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for webUrlapEllenorzesResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="webUrlapEllenorzesResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="webUrlapEllenorzesList" type="{http://tempuri.org/}ArrayOfWetUrlapEllenorzesElem" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dbId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tag" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "webUrlapEllenorzesResult", propOrder = {
    "webUrlapEllenorzesList",
    "status",
    "dbId",
    "tag"
})
public class WebUrlapEllenorzesResult {

    protected ArrayOfWetUrlapEllenorzesElem webUrlapEllenorzesList;
    protected String status;
    protected int dbId;
    protected int tag;

    /**
     * Gets the value of the webUrlapEllenorzesList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetUrlapEllenorzesElem }
     *     
     */
    public ArrayOfWetUrlapEllenorzesElem getWebUrlapEllenorzesList() {
        return webUrlapEllenorzesList;
    }

    /**
     * Sets the value of the webUrlapEllenorzesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetUrlapEllenorzesElem }
     *     
     */
    public void setWebUrlapEllenorzesList(ArrayOfWetUrlapEllenorzesElem value) {
        this.webUrlapEllenorzesList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the dbId property.
     * 
     */
    public int getDbId() {
        return dbId;
    }

    /**
     * Sets the value of the dbId property.
     * 
     */
    public void setDbId(int value) {
        this.dbId = value;
    }

    /**
     * Gets the value of the tag property.
     * 
     */
    public int getTag() {
        return tag;
    }

    /**
     * Sets the value of the tag property.
     * 
     */
    public void setTag(int value) {
        this.tag = value;
    }

}
