
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WetSafeInvoiceDetailResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WetSafeInvoiceDetailResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="webSzamlaFej" type="{http://tempuri.org/}WetSzamlaFej" minOccurs="0"/>
 *         &lt;element name="wetSzamlaTetelList" type="{http://tempuri.org/}ArrayOfWetSzamlaTetel" minOccurs="0"/>
 *         &lt;element name="peldanyszam" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WetSafeInvoiceDetailResult", propOrder = {
    "webSzamlaFej",
    "wetSzamlaTetelList",
    "peldanyszam",
    "status"
})
public class WetSafeInvoiceDetailResult {

    protected WetSzamlaFej webSzamlaFej;
    protected ArrayOfWetSzamlaTetel wetSzamlaTetelList;
    protected int peldanyszam;
    protected String status;

    /**
     * Gets the value of the webSzamlaFej property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaFej }
     *     
     */
    public WetSzamlaFej getWebSzamlaFej() {
        return webSzamlaFej;
    }

    /**
     * Sets the value of the webSzamlaFej property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaFej }
     *     
     */
    public void setWebSzamlaFej(WetSzamlaFej value) {
        this.webSzamlaFej = value;
    }

    /**
     * Gets the value of the wetSzamlaTetelList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public ArrayOfWetSzamlaTetel getWetSzamlaTetelList() {
        return wetSzamlaTetelList;
    }

    /**
     * Sets the value of the wetSzamlaTetelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetSzamlaTetel }
     *     
     */
    public void setWetSzamlaTetelList(ArrayOfWetSzamlaTetel value) {
        this.wetSzamlaTetelList = value;
    }

    /**
     * Gets the value of the peldanyszam property.
     * 
     */
    public int getPeldanyszam() {
        return peldanyszam;
    }

    /**
     * Sets the value of the peldanyszam property.
     * 
     */
    public void setPeldanyszam(int value) {
        this.peldanyszam = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
