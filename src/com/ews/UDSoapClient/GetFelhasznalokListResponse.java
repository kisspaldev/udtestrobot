
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getFelhasznalokListResult" type="{http://tempuri.org/}rdsFelhasznalokListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFelhasznalokListResult"
})
@XmlRootElement(name = "getFelhasznalokListResponse")
public class GetFelhasznalokListResponse {

    protected RdsFelhasznalokListResult getFelhasznalokListResult;

    /**
     * Gets the value of the getFelhasznalokListResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsFelhasznalokListResult }
     *     
     */
    public RdsFelhasznalokListResult getGetFelhasznalokListResult() {
        return getFelhasznalokListResult;
    }

    /**
     * Sets the value of the getFelhasznalokListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsFelhasznalokListResult }
     *     
     */
    public void setGetFelhasznalokListResult(RdsFelhasznalokListResult value) {
        this.getFelhasznalokListResult = value;
    }

}
