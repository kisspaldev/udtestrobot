
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSafeSpeedInvoiceResult" type="{http://tempuri.org/}WetSafeSpeedInvoiceResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSafeSpeedInvoiceResult"
})
@XmlRootElement(name = "setSafeSpeedInvoiceResponse")
public class SetSafeSpeedInvoiceResponse {

    protected WetSafeSpeedInvoiceResult setSafeSpeedInvoiceResult;

    /**
     * Gets the value of the setSafeSpeedInvoiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public WetSafeSpeedInvoiceResult getSetSafeSpeedInvoiceResult() {
        return setSafeSpeedInvoiceResult;
    }

    /**
     * Sets the value of the setSafeSpeedInvoiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public void setSetSafeSpeedInvoiceResult(WetSafeSpeedInvoiceResult value) {
        this.setSafeSpeedInvoiceResult = value;
    }

}
