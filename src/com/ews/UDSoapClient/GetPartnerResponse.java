
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPartnerResult" type="{http://tempuri.org/}RdsPartnerek" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPartnerResult"
})
@XmlRootElement(name = "getPartnerResponse")
public class GetPartnerResponse {

    protected RdsPartnerek getPartnerResult;

    /**
     * Gets the value of the getPartnerResult property.
     * 
     * @return
     *     possible object is
     *     {@link RdsPartnerek }
     *     
     */
    public RdsPartnerek getGetPartnerResult() {
        return getPartnerResult;
    }

    /**
     * Sets the value of the getPartnerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsPartnerek }
     *     
     */
    public void setGetPartnerResult(RdsPartnerek value) {
        this.getPartnerResult = value;
    }

}
