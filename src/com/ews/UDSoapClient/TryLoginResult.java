
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tryLoginResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tryLoginResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdsFelhasznalo" type="{http://tempuri.org/}RdsFelhasznalok" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tryLoginResult", propOrder = {
    "rdsFelhasznalo",
    "status"
})
public class TryLoginResult {

    protected RdsFelhasznalok rdsFelhasznalo;
    protected String status;

    /**
     * Gets the value of the rdsFelhasznalo property.
     * 
     * @return
     *     possible object is
     *     {@link RdsFelhasznalok }
     *     
     */
    public RdsFelhasznalok getRdsFelhasznalo() {
        return rdsFelhasznalo;
    }

    /**
     * Sets the value of the rdsFelhasznalo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RdsFelhasznalok }
     *     
     */
    public void setRdsFelhasznalo(RdsFelhasznalok value) {
        this.rdsFelhasznalo = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
