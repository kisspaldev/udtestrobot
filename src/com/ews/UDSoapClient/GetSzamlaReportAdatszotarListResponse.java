
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSzamlaReportAdatszotarListResult" type="{http://tempuri.org/}wetAdatszotarElemResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSzamlaReportAdatszotarListResult"
})
@XmlRootElement(name = "getSzamlaReportAdatszotarListResponse")
public class GetSzamlaReportAdatszotarListResponse {

    protected WetAdatszotarElemResult getSzamlaReportAdatszotarListResult;

    /**
     * Gets the value of the getSzamlaReportAdatszotarListResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetAdatszotarElemResult }
     *     
     */
    public WetAdatszotarElemResult getGetSzamlaReportAdatszotarListResult() {
        return getSzamlaReportAdatszotarListResult;
    }

    /**
     * Sets the value of the getSzamlaReportAdatszotarListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetAdatszotarElemResult }
     *     
     */
    public void setGetSzamlaReportAdatszotarListResult(WetAdatszotarElemResult value) {
        this.getSzamlaReportAdatszotarListResult = value;
    }

}
