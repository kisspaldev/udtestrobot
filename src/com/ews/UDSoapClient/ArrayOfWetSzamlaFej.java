
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWetSzamlaFej complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWetSzamlaFej">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WetSzamlaFej" type="{http://tempuri.org/}WetSzamlaFej" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWetSzamlaFej", propOrder = {
    "wetSzamlaFej"
})
public class ArrayOfWetSzamlaFej {

    @XmlElement(name = "WetSzamlaFej", nillable = true)
    protected List<WetSzamlaFej> wetSzamlaFej;

    /**
     * Gets the value of the wetSzamlaFej property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wetSzamlaFej property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWetSzamlaFej().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WetSzamlaFej }
     * 
     * 
     */
    public List<WetSzamlaFej> getWetSzamlaFej() {
        if (wetSzamlaFej == null) {
            wetSzamlaFej = new ArrayList<WetSzamlaFej>();
        }
        return this.wetSzamlaFej;
    }

}
