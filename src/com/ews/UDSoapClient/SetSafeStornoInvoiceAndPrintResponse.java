
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSafeStornoInvoiceAndPrintResult" type="{http://tempuri.org/}WetSafeSpeedInvoiceResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSafeStornoInvoiceAndPrintResult"
})
@XmlRootElement(name = "setSafeStornoInvoiceAndPrintResponse")
public class SetSafeStornoInvoiceAndPrintResponse {

    protected WetSafeSpeedInvoiceResult setSafeStornoInvoiceAndPrintResult;

    /**
     * Gets the value of the setSafeStornoInvoiceAndPrintResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public WetSafeSpeedInvoiceResult getSetSafeStornoInvoiceAndPrintResult() {
        return setSafeStornoInvoiceAndPrintResult;
    }

    /**
     * Sets the value of the setSafeStornoInvoiceAndPrintResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public void setSetSafeStornoInvoiceAndPrintResult(WetSafeSpeedInvoiceResult value) {
        this.setSafeStornoInvoiceAndPrintResult = value;
    }

}
