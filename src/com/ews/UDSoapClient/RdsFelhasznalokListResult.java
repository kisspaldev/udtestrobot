
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rdsFelhasznalokListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rdsFelhasznalokListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rdsFelhasznalokList" type="{http://tempuri.org/}ArrayOfRdsFelhasznalok" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rdsFelhasznalokListResult", propOrder = {
    "rdsFelhasznalokList",
    "status"
})
public class RdsFelhasznalokListResult {

    protected ArrayOfRdsFelhasznalok rdsFelhasznalokList;
    protected String status;

    /**
     * Gets the value of the rdsFelhasznalokList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRdsFelhasznalok }
     *     
     */
    public ArrayOfRdsFelhasznalok getRdsFelhasznalokList() {
        return rdsFelhasznalokList;
    }

    /**
     * Sets the value of the rdsFelhasznalokList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRdsFelhasznalok }
     *     
     */
    public void setRdsFelhasznalokList(ArrayOfRdsFelhasznalok value) {
        this.rdsFelhasznalokList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
