
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFilRdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pFilWsfWRdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFilWsfAzon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pDtpFilWsfKelteStart" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="pDtpFilWsfKelteStop" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pFilRdpId",
    "pFilWsfWRdpNev",
    "pFilWsfAzon",
    "pDtpFilWsfKelteStart",
    "pDtpFilWsfKelteStop"
})
@XmlRootElement(name = "getSzamlaFejList")
public class GetSzamlaFejList {

    protected String pSid;
    protected int pFilRdpId;
    protected String pFilWsfWRdpNev;
    protected String pFilWsfAzon;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pDtpFilWsfKelteStart;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pDtpFilWsfKelteStop;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pFilRdpId property.
     * 
     */
    public int getPFilRdpId() {
        return pFilRdpId;
    }

    /**
     * Sets the value of the pFilRdpId property.
     * 
     */
    public void setPFilRdpId(int value) {
        this.pFilRdpId = value;
    }

    /**
     * Gets the value of the pFilWsfWRdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFilWsfWRdpNev() {
        return pFilWsfWRdpNev;
    }

    /**
     * Sets the value of the pFilWsfWRdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFilWsfWRdpNev(String value) {
        this.pFilWsfWRdpNev = value;
    }

    /**
     * Gets the value of the pFilWsfAzon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFilWsfAzon() {
        return pFilWsfAzon;
    }

    /**
     * Sets the value of the pFilWsfAzon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFilWsfAzon(String value) {
        this.pFilWsfAzon = value;
    }

    /**
     * Gets the value of the pDtpFilWsfKelteStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPDtpFilWsfKelteStart() {
        return pDtpFilWsfKelteStart;
    }

    /**
     * Sets the value of the pDtpFilWsfKelteStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPDtpFilWsfKelteStart(XMLGregorianCalendar value) {
        this.pDtpFilWsfKelteStart = value;
    }

    /**
     * Gets the value of the pDtpFilWsfKelteStop property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPDtpFilWsfKelteStop() {
        return pDtpFilWsfKelteStop;
    }

    /**
     * Sets the value of the pDtpFilWsfKelteStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPDtpFilWsfKelteStop(XMLGregorianCalendar value) {
        this.pDtpFilWsfKelteStop = value;
    }

}
