
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFelNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFelRdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pFelNev",
    "pFelRdpId"
})
@XmlRootElement(name = "getFelhasznalokList")
public class GetFelhasznalokList {

    protected String pSid;
    protected String pFelNev;
    protected int pFelRdpId;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pFelNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFelNev() {
        return pFelNev;
    }

    /**
     * Sets the value of the pFelNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFelNev(String value) {
        this.pFelNev = value;
    }

    /**
     * Gets the value of the pFelRdpId property.
     * 
     */
    public int getPFelRdpId() {
        return pFelRdpId;
    }

    /**
     * Sets the value of the pFelRdpId property.
     * 
     */
    public void setPFelRdpId(int value) {
        this.pFelRdpId = value;
    }

}
