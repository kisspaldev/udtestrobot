
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSafeInvoiceAndPrintByWsfIdResult" type="{http://tempuri.org/}WetSafeSpeedInvoiceResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSafeInvoiceAndPrintByWsfIdResult"
})
@XmlRootElement(name = "setSafeInvoiceAndPrintByWsfIdResponse")
public class SetSafeInvoiceAndPrintByWsfIdResponse {

    protected WetSafeSpeedInvoiceResult setSafeInvoiceAndPrintByWsfIdResult;

    /**
     * Gets the value of the setSafeInvoiceAndPrintByWsfIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public WetSafeSpeedInvoiceResult getSetSafeInvoiceAndPrintByWsfIdResult() {
        return setSafeInvoiceAndPrintByWsfIdResult;
    }

    /**
     * Sets the value of the setSafeInvoiceAndPrintByWsfIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSafeSpeedInvoiceResult }
     *     
     */
    public void setSetSafeInvoiceAndPrintByWsfIdResult(WetSafeSpeedInvoiceResult value) {
        this.setSafeInvoiceAndPrintByWsfIdResult = value;
    }

}
