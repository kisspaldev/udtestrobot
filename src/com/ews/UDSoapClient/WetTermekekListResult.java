
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wetTermekekListResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wetTermekekListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wetTermekekList" type="{http://tempuri.org/}ArrayOfWetTermek" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wetTermekekListResult", propOrder = {
    "wetTermekekList",
    "status"
})
public class WetTermekekListResult {

    protected ArrayOfWetTermek wetTermekekList;
    protected String status;

    /**
     * Gets the value of the wetTermekekList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public ArrayOfWetTermek getWetTermekekList() {
        return wetTermekekList;
    }

    /**
     * Sets the value of the wetTermekekList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWetTermek }
     *     
     */
    public void setWetTermekekList(ArrayOfWetTermek value) {
        this.wetTermekekList = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
