
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setSzamlaResult" type="{http://tempuri.org/}webUrlapEllenorzesResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setSzamlaResult"
})
@XmlRootElement(name = "setSzamlaResponse")
public class SetSzamlaResponse {

    protected WebUrlapEllenorzesResult setSzamlaResult;

    /**
     * Gets the value of the setSzamlaResult property.
     * 
     * @return
     *     possible object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public WebUrlapEllenorzesResult getSetSzamlaResult() {
        return setSzamlaResult;
    }

    /**
     * Sets the value of the setSzamlaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public void setSetSzamlaResult(WebUrlapEllenorzesResult value) {
        this.setSzamlaResult = value;
    }

}
