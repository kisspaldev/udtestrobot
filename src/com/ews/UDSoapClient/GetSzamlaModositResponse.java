
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSzamlaModositResult" type="{http://tempuri.org/}wetSzamlaModositResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSzamlaModositResult"
})
@XmlRootElement(name = "getSzamlaModositResponse")
public class GetSzamlaModositResponse {

    protected WetSzamlaModositResult getSzamlaModositResult;

    /**
     * Gets the value of the getSzamlaModositResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaModositResult }
     *     
     */
    public WetSzamlaModositResult getGetSzamlaModositResult() {
        return getSzamlaModositResult;
    }

    /**
     * Sets the value of the getSzamlaModositResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaModositResult }
     *     
     */
    public void setGetSzamlaModositResult(WetSzamlaModositResult value) {
        this.getSzamlaModositResult = value;
    }

}
