
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pSid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRdpNev" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRdpFelhasznalo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRdpRdpId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSid",
    "pRdpNev",
    "pRdpFelhasznalo",
    "pRdpRdpId"
})
@XmlRootElement(name = "getPartnerList")
public class GetPartnerList {

    protected String pSid;
    protected String pRdpNev;
    protected String pRdpFelhasznalo;
    protected int pRdpRdpId;

    /**
     * Gets the value of the pSid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSid() {
        return pSid;
    }

    /**
     * Sets the value of the pSid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSid(String value) {
        this.pSid = value;
    }

    /**
     * Gets the value of the pRdpNev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRdpNev() {
        return pRdpNev;
    }

    /**
     * Sets the value of the pRdpNev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRdpNev(String value) {
        this.pRdpNev = value;
    }

    /**
     * Gets the value of the pRdpFelhasznalo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRdpFelhasznalo() {
        return pRdpFelhasznalo;
    }

    /**
     * Sets the value of the pRdpFelhasznalo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRdpFelhasznalo(String value) {
        this.pRdpFelhasznalo = value;
    }

    /**
     * Gets the value of the pRdpRdpId property.
     * 
     */
    public int getPRdpRdpId() {
        return pRdpRdpId;
    }

    /**
     * Sets the value of the pRdpRdpId property.
     * 
     */
    public void setPRdpRdpId(int value) {
        this.pRdpRdpId = value;
    }

}
