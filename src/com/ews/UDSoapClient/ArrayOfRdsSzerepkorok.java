
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRdsSzerepkorok complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRdsSzerepkorok">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RdsSzerepkorok" type="{http://tempuri.org/}RdsSzerepkorok" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRdsSzerepkorok", propOrder = {
    "rdsSzerepkorok"
})
public class ArrayOfRdsSzerepkorok {

    @XmlElement(name = "RdsSzerepkorok", nillable = true)
    protected List<RdsSzerepkorok> rdsSzerepkorok;

    /**
     * Gets the value of the rdsSzerepkorok property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rdsSzerepkorok property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRdsSzerepkorok().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RdsSzerepkorok }
     * 
     * 
     */
    public List<RdsSzerepkorok> getRdsSzerepkorok() {
        if (rdsSzerepkorok == null) {
            rdsSzerepkorok = new ArrayList<RdsSzerepkorok>();
        }
        return this.rdsSzerepkorok;
    }

}
