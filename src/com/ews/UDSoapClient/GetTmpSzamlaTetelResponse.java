
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTmpSzamlaTetelResult" type="{http://tempuri.org/}WetSzamlaTetel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTmpSzamlaTetelResult"
})
@XmlRootElement(name = "getTmpSzamlaTetelResponse")
public class GetTmpSzamlaTetelResponse {

    protected WetSzamlaTetel getTmpSzamlaTetelResult;

    /**
     * Gets the value of the getTmpSzamlaTetelResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetSzamlaTetel }
     *     
     */
    public WetSzamlaTetel getGetTmpSzamlaTetelResult() {
        return getTmpSzamlaTetelResult;
    }

    /**
     * Sets the value of the getTmpSzamlaTetelResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetSzamlaTetel }
     *     
     */
    public void setGetTmpSzamlaTetelResult(WetSzamlaTetel value) {
        this.getTmpSzamlaTetelResult = value;
    }

}
