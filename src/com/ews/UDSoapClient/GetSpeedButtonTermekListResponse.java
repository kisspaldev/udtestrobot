
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSpeedButtonTermekListResult" type="{http://tempuri.org/}wetTermekekListResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSpeedButtonTermekListResult"
})
@XmlRootElement(name = "getSpeedButtonTermekListResponse")
public class GetSpeedButtonTermekListResponse {

    protected WetTermekekListResult getSpeedButtonTermekListResult;

    /**
     * Gets the value of the getSpeedButtonTermekListResult property.
     * 
     * @return
     *     possible object is
     *     {@link WetTermekekListResult }
     *     
     */
    public WetTermekekListResult getGetSpeedButtonTermekListResult() {
        return getSpeedButtonTermekListResult;
    }

    /**
     * Sets the value of the getSpeedButtonTermekListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WetTermekekListResult }
     *     
     */
    public void setGetSpeedButtonTermekListResult(WetTermekekListResult value) {
        this.getSpeedButtonTermekListResult = value;
    }

}
