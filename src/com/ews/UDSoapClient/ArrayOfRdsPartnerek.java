
package com.ews.UDSoapClient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRdsPartnerek complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRdsPartnerek">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RdsPartnerek" type="{http://tempuri.org/}RdsPartnerek" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRdsPartnerek", propOrder = {
    "rdsPartnerek"
})
public class ArrayOfRdsPartnerek {

    @XmlElement(name = "RdsPartnerek", nillable = true)
    protected List<RdsPartnerek> rdsPartnerek;

    /**
     * Gets the value of the rdsPartnerek property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rdsPartnerek property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRdsPartnerek().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RdsPartnerek }
     * 
     * 
     */
    public List<RdsPartnerek> getRdsPartnerek() {
        if (rdsPartnerek == null) {
            rdsPartnerek = new ArrayList<RdsPartnerek>();
        }
        return this.rdsPartnerek;
    }

}
