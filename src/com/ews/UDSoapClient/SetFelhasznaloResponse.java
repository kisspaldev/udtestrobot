
package com.ews.UDSoapClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setFelhasznaloResult" type="{http://tempuri.org/}webUrlapEllenorzesResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setFelhasznaloResult"
})
@XmlRootElement(name = "setFelhasznaloResponse")
public class SetFelhasznaloResponse {

    protected WebUrlapEllenorzesResult setFelhasznaloResult;

    /**
     * Gets the value of the setFelhasznaloResult property.
     * 
     * @return
     *     possible object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public WebUrlapEllenorzesResult getSetFelhasznaloResult() {
        return setFelhasznaloResult;
    }

    /**
     * Sets the value of the setFelhasznaloResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebUrlapEllenorzesResult }
     *     
     */
    public void setSetFelhasznaloResult(WebUrlapEllenorzesResult value) {
        this.setFelhasznaloResult = value;
    }

}
