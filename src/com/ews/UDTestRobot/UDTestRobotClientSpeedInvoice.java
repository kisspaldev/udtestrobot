package com.ews.UDTestRobot;

import com.ews.UDSoapClient.WetSafeSpeedBillInput;
import com.ews.UDSoapClient.WetSafeSpeedInvoiceInput;
import com.ews.UDSoapClient.WetSafeSpeedInvoiceResult;

public class UDTestRobotClientSpeedInvoice extends A_TestRobotClient {

	@Override
	public String createTestItem() {
		WetSafeSpeedInvoiceInput safeSpeedInvoiceInput = new WetSafeSpeedInvoiceInput();
		
		safeSpeedInvoiceInput.setWsfVRdpnev("Teszt �gyf�l");
		safeSpeedInvoiceInput.setWsfVRdpadoszam("012345678");
		safeSpeedInvoiceInput.setWsfVRdpeuadoszam("EU012345678");
		safeSpeedInvoiceInput.setWsfVRdpbankszamla("12345678-12345678-12345678");
		safeSpeedInvoiceInput.setWsfVRdporszag("HUNGARY");
		safeSpeedInvoiceInput.setWsfVRdpiranyitoszam("1810");
		safeSpeedInvoiceInput.setWsfVRdpvaros("Budapest");
		safeSpeedInvoiceInput.setWsfVRdputcahazszam("Szabads�g t�r 18.");
		safeSpeedInvoiceInput.setWsfRendszam("R-"+ getRandomString(4));		
		safeSpeedInvoiceInput.setWsfSumBrutto(getRandomCost());
		safeSpeedInvoiceInput.setWstWetid(34);
		safeSpeedInvoiceInput.setWsfNyelv("HUN");

		WetSafeSpeedInvoiceResult result = wsWebSzamlaSoap.setSafeSpeedInvoice(getCurrentSid(), safeSpeedInvoiceInput);
		
		if (!result.getStatus().equals("OK")) {
			System.out.println("SpeedInvoice: Thread error [" + Thread.currentThread().getId() + "] " + result.getStatus());
		}
		
		return result.getStatus();
	}

	
}
