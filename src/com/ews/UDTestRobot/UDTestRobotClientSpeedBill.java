package com.ews.UDTestRobot;

import com.ews.UDSoapClient.WetSafeSpeedBillInput;
import com.ews.UDSoapClient.WetSafeSpeedInvoiceResult;

public class UDTestRobotClientSpeedBill extends A_TestRobotClient {
	
	public String createTestItem() {
		WetSafeSpeedBillInput safeSpeedBillInput = new WetSafeSpeedBillInput();
		safeSpeedBillInput.setWsfNyelv("HUN");
		safeSpeedBillInput.setWsfRendszam("R-"+ getRandomString(4));
		safeSpeedBillInput.setWsfSumBrutto(getRandomCost());
		safeSpeedBillInput.setWstWetid(34);
		
		WetSafeSpeedInvoiceResult result = wsWebSzamlaSoap.setSafeSpeedBill(getCurrentSid(), safeSpeedBillInput);
		
		if (!result.getStatus().equals("OK")) {
			System.out.println("SpeedBill: Thread error [" + Thread.currentThread().getId() + "] " + result.getStatus());
		}
		
		return result.getStatus();
	}
	
}
