package com.ews.UDTestRobot;

import java.util.Random;

import com.ews.UDSoapClient.TryLoginResult;
import com.ews.UDSoapClient.WsWebSzamla;
import com.ews.UDSoapClient.WsWebSzamlaSoap;

public abstract class A_TestRobotClient {
	protected WsWebSzamla wsWebSzamla;
	protected WsWebSzamlaSoap wsWebSzamlaSoap;
	protected TryLoginResult rdsTryLoginResult;
	protected Random rand;
		
	public A_TestRobotClient() {
		wsWebSzamla = new WsWebSzamla();
		wsWebSzamlaSoap = wsWebSzamla.getWsWebSzamlaSoap();
		rand =  new Random();
	}

	public void login() {
		rdsTryLoginResult = wsWebSzamlaSoap.rdsTryLogin("WP0000001", "developer", "almafa", "HUN");
	}
	
	public TryLoginResult getLoginResult() {
		return rdsTryLoginResult;
	}

	public String getCurrentSid() {
		if (rdsTryLoginResult==null) {
			return null;
		}
		
		return rdsTryLoginResult.getStatus();
	}
	
	protected String getRandomString(int length) {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	
	protected int getRandomCost() {
		return rand.nextInt(10000) + 1;
	}

	public void createRandomCountTestItem() {		
		int count = rand.nextInt(200) + 30;
		System.out.println("Thread start [" + Thread.currentThread().getId() + "] count: " + count);
		for (int i = 0; i<count; i++) {
			createTestItem();
		}
		System.out.println("Thread end [" + Thread.currentThread().getId() + "]");
	}

	
	public abstract String createTestItem();
	
}
