import java.util.ArrayList;
import java.util.List;

import com.ews.UDTestRobot.*;

public class UDTestRobot {

	public static void main(String[] args) {
		System.out.println("Start UDTestRobot");
		/*UDTestRobotClientSpeedInvoice udTestRobotClient = new UDTestRobotClientSpeedInvoice();
		udTestRobotClient.login();
		System.out.println("safeInvoice: " + udTestRobotClient.createTestItem());
		udTestRobotClient.createRandomCountTestItem();		
		*/
		
		
		/*List<UDTestRobotForSpeedInvoiceThread> testRobots = new ArrayList<UDTestRobotForSpeedInvoiceThread>();
		UDTestRobotForSpeedInvoiceThread udTestRobotItem;
		for(int i = 0; i<25; i++) {
			udTestRobotItem = new UDTestRobotForSpeedInvoiceThread();
			testRobots.add(udTestRobotItem);
		}
		
		for(UDTestRobotForSpeedInvoiceThread udTestRobotThread : testRobots) {
			udTestRobotThread.start();
		}*/
		

		List<Thread> testRobots = new ArrayList<Thread>();
		UDTestRobotForSpeedInvoiceThread udTestRobotInvoiceItem;
		UDTestRobotForSpeedBillThread udTestRobotBillItem;
		
		Boolean currentIsBill = Boolean.FALSE;
		
		for(int i = 0; i<30; i++) {
			if (currentIsBill) {
				udTestRobotBillItem = new UDTestRobotForSpeedBillThread();
				testRobots.add(udTestRobotBillItem);
			}
			else {
				udTestRobotInvoiceItem = new UDTestRobotForSpeedInvoiceThread();
				testRobots.add(udTestRobotInvoiceItem);
			}			
			
			currentIsBill = !currentIsBill;
		}
		
		for(Thread udTestRobotThread : testRobots) {
			udTestRobotThread.start();
		}		

	}

}
